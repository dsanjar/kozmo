<?php namespace Kozmo\Helpers;

/**
 * Payment helper
 */
class PaymentHelper
{
	public static function getCurrencies()
	{
		return [
            398 => 'KZT',
            840 => 'USD',
            643 => 'RUB'
        ];
	}

	public static function getCurrenciesTitles()
	{
		return [
            398 => 'тенге',
            840 => 'доллары',
            643 => 'рубли'
        ];
	}

	public static function getProducts()
	{
		return [
			'sport' => 'Kozmo\Sport\Models\Product',
			'yoga' => 'Kozmo\Sport\Models\Product',
			'food' => 'Kozmo\Food\Models\Product'
		];
	}

	public static function getModelByType($type = null)
	{
		if (!$type) {
			return;
		}

		$products = self::getProducts();
		if (!isset($products[$type])) {
			return;
		}

		return $products[$type];
	}

	public static function findModelByType($id = null, $type = null)
	{
		if (!$id || !$type) {
			return;
		}
		if (!$model = self::getModelByType($type)) {
			return;
		}

		if (!class_exists($model)) {
            return null;
        }
        if (!$product = call_user_func_array($model.'::find', [$id])) {
            return null;
        }
        return $product;
	}

	public static function getAgents()
	{
		return ['mobile', 'web', 'admin', 'crm'];
	}

	public static function checkAgent($agent = null)
	{
		if (!$agent) {
			return;
		}
		$agents = self::getAgents();
		return in_array($agent, $agents);
	}

	public static function isAgentMobile($agent = null)
	{
		if (!$agent) {
			return;
		}
		return $agent == 'mobile';
	}

	public static function isAgentWeb($agent = null)
	{
		if (!$agent) {
			return;
		}
		return $agent == 'web';
	}
}
