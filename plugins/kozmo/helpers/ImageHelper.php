<?php namespace Kozmo\Helpers;

/**
 * Image helper
 */
class ImageHelper
{
		public static function toBase64($path)
		{
			$type = pathinfo($path, PATHINFO_EXTENSION);
			$data = file_get_contents($path);
			$base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
			return $base64;
		}

		public static function publicUrl($url)
		{
			$host = $_SERVER['HTTP_HOST'];
			return 'https://'.$host.'/storage/app/media'.$url;
		}

		public static function qrcodeUrl($url)
		{
			$host = $_SERVER['HTTP_HOST'];
			return 'https://'.$host.'/storage/app/'.$url;
		}
}
