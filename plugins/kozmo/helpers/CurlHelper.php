<?php namespace Kozmo\Helpers;

use Log;

class CurlHelper
{
	public $url;

	public function __construct($url = null)
	{
		if (!empty($url)) {
			$this->url = $url;
		}
	}

	public function setUrl($value)
	{
		if (!empty($value)) {
			$this->url = $value;
		}
	}

	public function getUrl()
	{
		return $this->url;
	}

    public function headers($headers = [])
    {
        $flat = [];
        if (!empty($headers)) {
            foreach ($headers as $key => $value) {
                $flat[] = $key.': '.$value;
            }
        }
        return $flat;
    }

    public function params($params = [])
    {
        $params = $this->sign($params);
        $params = http_build_query($params);
        return $params;
    }

    public function sign($params = [])
    {
        return $params;
    }

    public function url($routes = [], $url = null)
    {
        if (!$url) {
            $url = $this->url;
        }
        if (!empty($routes)) {
            $url .= '/'.implode('/', $routes);
        }
        return $url;
    }

    public function response($response)
    {
        return $response;
    }

	public function request($url, $headers = [])
    {
		$request = curl_init();
        curl_setopt($request, CURLOPT_URL, $url);
        curl_setopt($request, CURLOPT_HEADER, true);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($request, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($request, CURLOPT_FOLLOWLOCATION, false);
		curl_setopt($request, CURLOPT_HTTPHEADER, $headers);
        return $request;
	}

    public function post($routes = [], $params = [], $headers = [], $info = false)
    {
        $headers    = $this->headers($headers);
        $params     = $this->params($params);
        $url        = $this->url($routes);
        $request    = $this->request($url, $headers);
        curl_setopt($request, CURLOPT_POST, true);
        curl_setopt($request, CURLOPT_POSTFIELDS, $params);
        $response   = $this->response($info?$this->info($request):$this->exec($request));

		Log::info(json_encode($params));
        return $response;
    }

    public function get($routes = [], $params = [], $headers = [], $info = false)
    {
        $headers    = $this->headers($headers);
        $params     = $this->params($params);
        $url        = $this->url($routes);
        $request    = $this->request($url.'?'.$params, $headers);
        $response   = $this->response($info?$this->info($request):$this->exec($request));
        return $response;
    }

    public function exec($request)
    {
        try {
            $response = curl_exec($request);
        } catch (\Exception $e) {
            return;
        } catch (\ApplicationException $e) {
            return;
        }
		$info = curl_getinfo($request);
        return $response;
    }

	public function info($request)
	{
		try {
            $response = curl_exec($request);
        } catch (\Exception $e) {
            return;
        } catch (\ApplicationException $e) {
            return;
        }
        return curl_getinfo($request);
	}
}
