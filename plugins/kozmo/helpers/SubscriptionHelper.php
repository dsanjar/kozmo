<?php namespace Kozmo\Helpers;

/**
 * Subscription helper
 */
class SubscriptionHelper
{
	const UNITS_VISITS = 'visits';
    const UNITS_PERIOD = 'period';

	public static function getUnitsTitle($units, $limit)
    {
        $titles = [
            self::UNITS_VISITS => ['посещение', 'посещения', 'посещений'],
            self::UNITS_PERIOD => ['день', 'дня', 'дней']
        ];

        $current_titles = $titles[$units];
        $current_limit = substr($limit, -1);

        if (($current_limit == 1) || ($limit == 11)) {
            $units_title = $current_titles[0];
        } elseif (($current_limit > 1) && ($current_limit < 5)) {
            $units_title = $current_titles[1];
        } elseif (($current_limit > 5) || ($current_limit <= 0) || ($limit <= 0)) {
            $units_title = $current_titles[2];
        } else {
            $units_title = $current_titles[0];
        }

        return $units_title;
    }

	public static function getVisitsTitle($value)
    {
        return self::getUnitsTitle(self::UNITS_VISITS, $value);
    }

    public static function getPeriodTitle($value)
    {
        return self::getUnitsTitle(self::UNITS_PERIOD, $value);
    }
}
