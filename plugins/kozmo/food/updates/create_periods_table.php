<?php namespace Kozmo\Food\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePeriodsTable extends Migration
{
    public function up()
    {
        Schema::create('kozmo_food_periods', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('kozmo_food_periods');
    }
}
