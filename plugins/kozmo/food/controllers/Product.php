<?php namespace Kozmo\Food\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Product Back-end Controller
 */
class Product extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Kozmo.Food', 'food', 'product');
    }
}
