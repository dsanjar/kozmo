<?php namespace Kozmo\Food;

use Backend;
use System\Classes\PluginBase;

/**
 * Food Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Food',
            'description' => 'No description provided yet...',
            'author'      => 'Kozmo',
            'icon'        => 'icon-coffee'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Kozmo\Food\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'kozmo.food.some_permission' => [
                'tab' => 'Food',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'food' => [
                'label'       => 'Food',
                'url'         => Backend::url('kozmo/food/mycontroller'),
                'icon'        => 'icon-coffee',
                'permissions' => ['kozmo.food.*'],
                'order'       => 500,
            ],
        ];
    }
}
