<?php namespace Kozmo\Api\Classes;

use Carbon\Carbon;
use Kozmo\Api\Classes\ResponseManager;
use Endroid\QrCode\QrCode as EndroidQrCode;
use Illuminate\Support\Facades\Storage;
use Kozmo\Api\Classes\ApiHelper;
use Kozmo\Common\Models\QrCode as QrCodeModel;
use Illuminate\Support\Facades\DB;
use Kozmo\Personal\Models\Checkin;
use Kozmo\Personal\Models\User;

class QrCode
{
    const FILENAME_PREFIX           = 'user_qr_code_';
    const FILENAME_PRINTABLE_PREFIX = 'printable_';
    const FOLDER_PATH               = 'user-qr-codes/';
    const FILE_EXTENSION            = '.png';
    const PRINTABLE_FILE_EXTENSION  = '.eps';

    public static function generate()
    {
        $post = post();
        if (!isset($post['amount'])) {
            return ApiHelper::error('qr-code-amount-empty');
        }
        $isPrintable = false;
        if (isset($post['is_printable'])) {
            $isPrintable = $post['is_printable'];
        }
        $amountIterative    = 0;
        $result             = [];
        $amount             = $post['amount'];
        $qrCodeModel        = QrCodeModel::orderBy('id', 'desc')->first();
        $startFrom          = count($qrCodeModel) > 0 ? $qrCodeModel->id : 0;

        for ($i = 0; $i < $amount; $i++) {
            $response = self::generateQrCode($startFrom, $isPrintable);

            if (!$response) {
                return ApiHelper::error('qr-code');
            }

            $startFrom++;
            $amountIterative++;
            $result[] = $response;

            sleep(1);
        }

        return ResponseManager::success([
            'message'   => $amountIterative . ' QR codes successfully generated',
            'result'    => $result
        ]);
    }

    /**
     * Over 300 000 000 possible unique codes
     * @param $number
     * @return string
     */
    protected static function generateUniqueString($number) {
        $scrambled = (240049382 * $number + 37043083) % 308915753;
        return mb_strtoupper(base_convert($scrambled, 10, 26));
    }

    protected static function generateQrCode($number, $isPrintable)
    {
        $uniqueString   = self::generateUniqueString($number);
        $qrCodeFile     = self::saveQrCode($uniqueString, $isPrintable);

        if ($uniqueString && $qrCodeFile) {
            if (self::saveToDB($uniqueString, $qrCodeFile, $isPrintable)) {
                return [
                    'string_code'   => $uniqueString,
                    'qr_code'       => $qrCodeFile
                ];
            }

            return ApiHelper::error('qr-code-database');
        }

        return ApiHelper::error('qr-code');
    }

    protected static function saveQrCode($string, $is_printable = false)
    {
        $prefix = ($is_printable) ? self::FILENAME_PREFIX.self::FILENAME_PRINTABLE_PREFIX : self::FILENAME_PREFIX;
        $uniqueName = hash('md5', time(), false);
        $filename   = $prefix . $uniqueName . self::FILE_EXTENSION;
        if ($is_printable) {
            $filenamePrintable = $prefix . $uniqueName . self::PRINTABLE_FILE_EXTENSION;
        }

        if (!Storage::makeDirectory(self::FOLDER_PATH)) {
            return ApiHelper::error('qr-code-directory');
        }

        $qrCode  = new EndroidQrCode($string);
        $content = $qrCode->writeString();

        if (!Storage::put(self::FOLDER_PATH . $filename, $content)) {
            return ApiHelper::error('qr-code-file');
        }

        if ($is_printable) {
            $contentPrintable = $qrCode->setWriterByName('eps')->writeString();

            if (!Storage::put(self::FOLDER_PATH . $filenamePrintable, $contentPrintable)) {
                return ApiHelper::error('qr-code-file');
            }
        }

        return self::FOLDER_PATH . $filename;
    }

    protected static function saveToDB($unique_string, $image_path, $is_printable = false)
    {
        $qrCode = new QrCodeModel();
        $qrCode->unique_string  = $unique_string;
        $qrCode->image_path     = $image_path;
        $qrCode->is_printable   = $is_printable;

        if ($qrCode->save() === false) {
            return ApiHelper::error('qr-code-database');
        }

        return true;
    }

    // public static function checkInAndOut()
    // {
    //     $post           = post();
    //     $unique_string  = $post['unique_string'];
    //     $status         = $post['status'];
    //     $scanner_id     = $post['scanner_id'];
    //
    //     $userModel = User::where('persist_code', $unique_string)->first();
    //
    //     if (count($userModel) === 0) {
    //         return ResponseManager::error('qrcode-checkin-user-not-found');
    //     }
    //
    //     $checkinModel                   = new Checkin();
    //     $checkinModel->user_id          = $userModel->id;
    //     $checkinModel->scanner_id       = $scanner_id;
    //     $checkinModel->status           = $status;
    //     $checkinModel->checked_in_at    = $status === 'checkin' ? Carbon::now() : $checkinModel->checked_in_at;
    //     $checkinModel->checked_out_at   = $status === 'checkout' ? Carbon::now() : $checkinModel->checked_out_at;
    //
    //     if ($checkinModel->save() === false) {
    //         return ResponseManager::error('qr-code-checkin-database-failure');
    //     }
    //
    //     return ResponseManager::success([
    //         'user' => $userModel
    //     ]);
    // }
}
