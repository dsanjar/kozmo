<?php namespace Kozmo\Api\Middleware;

use Log;
use Request;
use Closure;
use Kozmo\Api\Classes\ApiHelper;
use Kozmo\Api\Classes\ResponseManager;

class CheckApiToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!env('API_USE_MIDDLEWARE')) {
            return $next($request);
        }
        if (!$token = $request->header('api-token')) {
            return ResponseManager::error('api');
        }
        if (!ApiHelper::checkToken($token)) {
            return ResponseManager::error('api');
        }
        Log::debug(Request::url().' with '.json_encode(post()));
        return $next($request);
    }

}
