<?php namespace Kozmo\Api\Middleware;

use Closure;
use Kozmo\Api\Classes\AuthManager;
use Kozmo\Api\Classes\ResponseManager;

class CheckAuthToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!env('API_USE_MIDDLEWARE')) {
            return $next($request);
        }
        if (!$token = $request->header('auth-token')) {
            return ResponseManager::error('auth');
        }
        if (!AuthManager::instance()->checkJWT()) {
            return ResponseManager::error('auth');
        }
        if (!$user = AuthManager::instance()->getUserByJWT()) {
            return ResponseManager::error('auth');
        }

        $request['user'] = $user;

        return $next($request);
    }

}
