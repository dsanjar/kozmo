<?php namespace Kozmo\Personal;

use Backend;
use System\Classes\PluginBase;

/**
 * Personal Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Personal',
            'description' => 'No description provided yet...',
            'author'      => 'Kozmo',
            'icon'        => 'icon-users'
        ];
    }

    public function registerSettings()
    {
        return [
            'settings' => [
                'category'    => 'Personal',
                'label'       => 'Settings',
                'description' => 'Manage personal settings.',
                'icon'        => 'icon-plug',
                'class'       => 'Kozmo\Personal\Models\Settings',
                'order'       => 500,
                'keywords'    => 'api personal',
                'permissions' => ['kozmo.personal.settings']
            ]
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Kozmo\Personal\Components\UserForm' => 'userForm',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'kozmo.personal.common' => [
                'tab' => 'Personal',
                'label' => 'Common permission'
            ],
            'kozmo.personal.user' => [
                'tab' => 'Personal',
                'label' => 'User permission'
            ],
            'kozmo.personal.subs' => [
                'tab' => 'Personal',
                'label' => 'Subs permission'
            ],
            'kozmo.personal.checkin' => [
                'tab' => 'Personal',
                'label' => 'Checkin permission'
            ],
            'kozmo.personal.smscodes' => [
                'tab' => 'Personal',
                'label' => 'SMS codes permission'
            ],
            'kozmo.personal.settings' => [
                'tab' => 'Personal',
                'label' => 'Settings permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'personal' => [
                'label'       => 'Personal',
                'url'         => Backend::url('kozmo/personal/user'),
                'icon'        => 'icon-users',
                'permissions' => ['kozmo.personal.common'],
                'order'       => 200,
                'sideMenu' => [
                    'users' => [
                        'label' => 'Users',
                        'url'   => Backend::url('kozmo/personal/user'),
                        'icon'  => 'icon-circle-thin',
                        'permissions' => ['kozmo.personal.user'],
                        'order' => 10
                    ],
                    'subscriptions' => [
                        'label' => 'Subscriptions',
                        'url'   => Backend::url('kozmo/personal/subscription'),
                        'icon'  => 'icon-circle-thin',
                        'permissions' => ['kozmo.personal.subs'],
                        'order' => 20
                    ],
                    'checkins' => [
                        'label' => 'Checkins',
                        'url'   => Backend::url('kozmo/personal/checkin'),
                        'icon'  => 'icon-circle-thin',
                        'permissions' => ['kozmo.personal.checkin'],
                        'order' => 30
                    ],
                    'smscodes' => [
                        'label' => 'SMS Codes',
                        'url'   => Backend::url('kozmo/personal/smscodes'),
                        'icon'  => 'icon-circle-thin',
                        'permissions' => ['kozmo.personal.smscodes'],
                        'order' => 40
                    ],
                ]
            ],
        ];
    }
}
