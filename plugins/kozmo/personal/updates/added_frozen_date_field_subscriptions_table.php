<?php namespace Kozmo\Personal\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddFrozenDateFieldToSubscriptionsTable extends Migration
{
    public function up()
    {
        Schema::table('kozmo_personal_subscriptions', function(Blueprint $table) {
            $table->timestamp('unfreez_at')->nullable();
        });
    }

    public function down()
    {
        Schema::table('kozmo_personal_subscriptions', function(Blueprint $table) {
            $table->dropColumn('unfreez_at');
        });
    }
}
