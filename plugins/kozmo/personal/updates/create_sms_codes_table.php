<?php namespace Kozmo\Personal\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateSmsCodesTable extends Migration
{
    public function up()
    {
        Schema::create('kozmo_personal_sms_codes', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('phone');
            $table->string('sms_code');
            $table->boolean('is_used')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('kozmo_personal_sms_codes');
    }
}
