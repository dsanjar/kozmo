<?php namespace Kozmo\Personal\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddSubsIdCheckinsTable extends Migration
{
    public function up()
    {
        Schema::table('kozmo_personal_checkins', function(Blueprint $table) {
            $table->integer('subscription_id')->nullable();
        });
    }

    public function down()
    {
        Schema::table('kozmo_personal_checkins', function(Blueprint $table) {
            $table->dropColumn('subscription_id')->nullable();
        });
    }
}
