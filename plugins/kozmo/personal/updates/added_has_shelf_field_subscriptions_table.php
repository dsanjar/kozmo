<?php namespace Kozmo\Personal\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddHasShelfFieldToSubscriptionsTable extends Migration
{
    public function up()
    {
        Schema::table('kozmo_personal_subscriptions', function(Blueprint $table) {
            $table->boolean('has_shelf')->default(0);
            $table->boolean('has_sportswear')->default(0);
        });
    }

    public function down()
    {
        Schema::table('kozmo_personal_subscriptions', function(Blueprint $table) {
            $table->dropColumn('has_shelf');
            $table->dropColumn('has_sportswear');
        });
    }
}
