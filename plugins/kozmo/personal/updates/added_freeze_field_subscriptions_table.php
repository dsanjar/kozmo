<?php namespace Kozmo\Personal\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddFreezeFieldToSubscriptionsTable extends Migration
{
    public function up()
    {
        Schema::table('kozmo_personal_subscriptions', function(Blueprint $table) {
            $table->boolean('is_frozen')->default(0);
            $table->boolean('has_freeze')->default(0);
            $table->integer('freeze_limit')->nullable();
        });
    }

    public function down()
    {
        Schema::table('kozmo_personal_subscriptions', function(Blueprint $table) {
            $table->dropColumn('is_frozen');
            $table->dropColumn('has_freeze');
            $table->dropColumn('freeze_limit');
        });
    }
}
