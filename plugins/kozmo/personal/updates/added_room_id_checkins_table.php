<?php namespace Kozmo\Personal\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddRoomIdCheckinsTable extends Migration
{
    public function up()
    {
        Schema::table('kozmo_personal_checkins', function(Blueprint $table) {
            $table->integer('room_id')->nullable();
        });
    }

    public function down()
    {
        Schema::table('kozmo_personal_checkins', function(Blueprint $table) {
            $table->dropColumn('room_id')->nullable();
        });
    }
}
