<?php namespace Kozmo\Personal\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('kozmo_personal_users', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('surname', 45);
            $table->string('name', 45);
            $table->string('phone', 45);
            $table->string('email', 45)->nullable();
            $table->string('gender', 45)->nullable();
            $table->string('age', 45)->nullable();
            $table->string('sms_code', 45)->nullable();
            $table->string('persist_code', 45)->nullable();
            $table->boolean('is_enabled')->default(0);

            $table->unique('phone');
            $table->unique('email');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('kozmo_personal_users');
    }
}
