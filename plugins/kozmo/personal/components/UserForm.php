<?php namespace Kozmo\Personal\Components;

use Redirect;
use ApplicationException;
use Kozmo\Personal\Models\User;
use Kozmo\Payment\Models\Order;
use Kozmo\Api\Classes\AuthManager;
use Kozmo\Helpers\PaymentHelper;
use Kozmo\Payment\Classes\PaymentManager;
use Kozmo\Sport\Models\Product as SportProduct;
use Cms\Classes\ComponentBase;
use Kozmo\Payment\Models\Settings;

class UserForm extends ComponentBase
{
    private $defaultCurrency;

    public function componentDetails()
    {
        return [
            'name'        => 'UserForm Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function init()
	{
		$this->defaultCurrency 	= Settings::get('default_currency');
	}

    public function onRun()
    {
        $products = SportProduct::published()->get();
        $this->page['products'] = $products;
    }

    public function onSignup()
    {
        $post = post();
        $authManager = AuthManager::instance();
        $paymentManager = PaymentManager::instance();

        if (isset($post['phone'])) {
            $phone = trim(str_replace(' ', '', $post['phone']));
            $phone = str_replace('(', '', $phone);
            $phone = str_replace(')', '', $phone);
            $phone = str_replace('-', '', $phone);
        }
        if (!$authManager->phoneValidate($phone)) {
            throw new ApplicationException('Phone validation error.');
        }

        if (!$user = User::byPhone($phone)->enabled()->first()) {
            $result = $authManager->signUp(true);
            if (!is_array($result) || !isset($result['user'])) {
                throw new ApplicationException('AuthManager signup error.');
            }
            $user = $result['user'];
        }

        if (!$agent = post('agent')) {
			return;
		}
		if (!PaymentHelper::checkAgent($agent)) {
			throw new ApplicationException('Check agent error.');
		}
		if (!$productId = post('product_id')) {
			throw new ApplicationException('Empty product id.');
		}
		if (!$productType = post('product_type')) {
			throw new ApplicationException('Empty product type.');
		}
		if (!$product = PaymentHelper::findModelByType($productId, $productType)) {
			throw new ApplicationException('Empty product.');
		}
		if (!$user->checkSubscriptions($product)) {
			throw new ApplicationException('Check subs error.');
		}
		if (!$productModel = PaymentHelper::getModelByType($productType)) {
			throw new ApplicationException('Empty product model.');
		}
		if (!$order = Order::byKeys($user->id, $productId, $productType)->waiting()->first()) {
			$order = new Order;
		}
		$order->user_id = $user->id;
		$order->product_id = $productId;
		$order->product_type = $productType;
		$order->product_model = $productModel;
		$order->currency_id = $this->defaultCurrency;
		$order->save();

		if (!$user->checkSubscriptions($order->product)) {
			throw new ApplicationException('Double check subs error.');
		}

        $order->setAccept();
        $paymentManager->assign($user, $order);

        return Redirect::to('/signup/success');
    }

}
