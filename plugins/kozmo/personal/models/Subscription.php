<?php namespace Kozmo\Personal\Models;

use Model;
use Carbon\Carbon;

/**
 * Subscription Model
 */
class Subscription extends Model
{
    const TYPE_SPORT = 'sport';
    const TYPE_YOGA = 'yoga';
    const TYPE_FOOD = 'food';

    const UNITS_VISITS = 'visits';
    const UNITS_PERIOD = 'period';
    const UNITS_FREEZE = 'freeze';

    /**
     * @var string The database table used by the model.
     */
    public $table = 'kozmo_personal_subscriptions';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $appends = ['visits_title', 'period_title'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Hidden fields
     */
    protected $hidden = ['product_model'];

    /**
     * @var array Dates fields
     */
    protected $dates = ['expired_at', 'unfreez_at'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'user' => ['Kozmo\Personal\Models\User', 'key' => 'user_id'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
    *   Scopes
    */
    public function scopeEnabled($query)
    {
      return $query->where('is_enabled', 1);
    }

    public function scopeFree($query)
    {
        return $query->where('is_free', 1);
    }

    public function scopeHasCount($query)
    {
        return $query->where('has_count', 1);
    }

    public function scopeHasPeriod($query)
    {
        return $query->where('has_period', 1);
    }

    public function scopeHasFreeze($query)
    {
        return $query->where('has_freeze', 1);
    }

    public function scopeFrozen($query)
    {
        return $query->where('is_frozen', 1);
    }

    public function scopeNotFrozen($query)
    {
        return $query->where('is_frozen', 0);
    }

    public function scopeByProductId($query, $value)
    {
        return $query->where('product_id', $value);
    }

    public function scopeByProductType($query, $value)
    {
        return $query->where('product_type', $value);
    }

    public function scopeByProductSport($query)
    {
        return $query->where('product_type', self::TYPE_SPORT);
    }

    public function scopeByProductYoga($query)
    {
        return $query->where('product_type', self::TYPE_YOGA);
    }
    /**
    *   Events
    */
    public function afterFetch()
    {
        if ($this->is_frozen) {
            $period = Carbon::now()->startOfDay()->diffInDays($this->unfreez_at, false);
            if ($period != $this->freeze_limit) {
                if ($period <= 0) {
                    $period = 0;
                    $this->has_freeze = false;
                }
                $this->freeze_limit = $period;
                $this->save();
            }
        } elseif ($this->has_period) {
            $period = Carbon::now()->startOfDay()->diffInDays($this->expired_at, false);
            if ($period != $this->period_limit) {
                if ($period <= 0) {
                    $period = 0;
                    $this->is_enabled = false;
                }
                $this->period_limit = $period;
                $this->save();
            }
        }
    }

    public function beforeSave()
    {
        if ($this->isDirty('product_id')) {
            $this->initByProduct();
        }
        if ($this->isDirty('visits_limit')) {
            if ($this->has_count) {
                if ($this->visits_limit <= 0) {
                    $this->visits_limit = 0;
                    $this->is_enabled = false;
                }
            }
        }
        if ($this->isDirty('period_limit')) {
            if ($this->has_period) {
                if ($this->period_limit <= 0) {
                    $this->period_limit = 0;
                    $this->is_enabled = false;
                }
            }
        }
    }

    /**
    *   Methods
    */
    public function getTypeOptions()
    {
        return [
            self::TYPE_YOGA => 'Kozmo Йога',
            self::TYPE_SPORT => 'Kozmo Спорт',
            self::TYPE_FOOD => 'Kozmo Питание'
        ];
    }

    /**
    *   Attributes
    */
    public function getProductAttribute($value)
    {
        if(!$this->product_model || !$this->product_id) {
            return null;
        }
        if (!class_exists($this->product_model)) {
            return null;
        }
        if (!$product = call_user_func_array($this->product_model.'::find', [$this->product_id])) {
            return null;
        }
        return $product;
    }

    public function initByProduct()
    {
        if (!$product = $this->product) {
            return;
        }
        $this->product_title = $product->title;
        $this->product_type = $product->type;

        $this->has_shelf = $product->has_shelf;
        $this->has_sportswear = $product->has_sportswear;

        $this->has_fresh = $product->has_fresh;
        $this->has_schedule = $product->has_schedule;

        $this->is_free = $product->is_free;
        $this->has_count = $product->has_count;
        $this->has_period = $product->has_period;
        $this->per_price = $product->per_price;

        $this->is_frozen = false;
        $this->has_freeze = $product->has_freeze;
        $this->freeze_limit = $product->freeze_limit;

        $this->visits_limit = $product->getVisitsLimit();
        $this->period_limit = $product->getPeriodLimit();
        $this->expired_at = $product->getExpiredDate();
    }

    public function getUnitsTitle($units, $limit)
    {
        $titles = [
            self::UNITS_VISITS => ['посещение', 'посещения', 'посещений'],
            self::UNITS_PERIOD => ['день', 'дня', 'дней'],
            self::UNITS_FREEZE => ['заморозка', 'заморозка', 'заморозка'],
        ];

        $current_titles = $titles[$units];
        $current_limit = substr($limit, -1);

        if (($current_limit == 1) || ($limit == 11)) {
            $units_title = $current_titles[0];
        } elseif (($current_limit > 1) && ($current_limit < 5)) {
            $units_title = $current_titles[1];
        } elseif (($current_limit > 5) || ($current_limit <= 0) || ($limit <= 0)) {
            $units_title = $current_titles[2];
        } else {
            $units_title = $current_titles[0];
        }

        return $units_title;
    }

    public function getVisitsTitleAttribute($value)
    {
        return $this->getUnitsTitle(self::UNITS_VISITS, $this->visits_limit);
    }

    public function getPeriodTitleAttribute($value)
    {
        return $this->getUnitsTitle(self::UNITS_PERIOD, $this->period_limit);
    }

    public function getFreezeTitleAttribute($value)
    {
        return $this->getUnitsTitle(self::UNITS_FREEZE, $this->period_limit);
    }

    /*
    * Methods
    */
    public function setFreeze()
    {
        if (!$this->has_freeze) {
            return;
        }
        if (!$this->freeze_limit) {
            return;
        }
        $this->is_frozen = 1;
        $this->unfreez_at = Carbon::now()->addDays($this->freeze_limit)->endOfDay();
        $this->save();
    }

    public function setUnfreeze()
    {
        $this->is_frozen = 0;
        $this->save();
    }
}
