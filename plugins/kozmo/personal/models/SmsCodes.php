<?php namespace Kozmo\Personal\Models;

use Model;
use Illuminate\Support\Facades\DB;

/**
 * sms_codes Model
 */
class SmsCodes extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'kozmo_personal_sms_codes';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['phone', 'sms_code', 'is_used'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'user' => ['Kozmo\Personal\Models\User', 'key' => 'phone', 'otherKey' => 'phone']
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function getLatest($key, $value, $isUsed)
    {
        // return DB::table('kozmo_personal_sms_codes')
        //     ->where($key, '=', $value)
        //     ->where('is_used', '=', $isUsed)
        //     ->orderBy('created_at', 'desc')
        //     ->limit(1)
        //     ->get();
    }

    /*
    *   Scopes
    */

    public function scopeByPhoneCode($query, $phone, $sms_code)
    {
        return $query->where('phone', $phone)->where('sms_code', $sms_code);
    }

    public function scopeByPhone($query, $phone)
    {
        return $query->where('phone', $phone);
    }

    public function scopeIsUsed($query)
    {
        return $query->where('is_used', 1);
    }

    public function scopeIsNotUsed($query)
    {
        return $query->where('is_used', 0);
    }

    public function scopeLatest($query)
    {
        return $query->orderBy('created_at', 'desc');
    }

    /*
    *   Attributes, methods
    */
    public function setUsed()
    {
        $this->is_used = 1;
        return $this->save();
    }
}
