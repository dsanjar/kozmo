<?php namespace Kozmo\Personal\Models;

use Model;
use Carbon\Carbon;
use Kozmo\Sport\Models\Fresh;
use Kozmo\Helpers\SubscriptionHelper;

/**
 * User Model
 */
class User extends Model
{
    const LIMIT_ROOMS_DAILY = 4;
    const LIMIT_ROOMS_DAYS  = 7;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'kozmo_personal_users';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Appends fields
     */
    protected $appends = [
        'fresh_data',
        'rooms_data',
        'qr_code_data',
        'subscription_data',
        'shelves_data',
        'has_fresh',
        'has_schedule',
        'has_subscription',
        'has_shelf',
        'has_sportswear',
        'has_freeze',
        'is_frozen',
        'counter_limits',
        'counter_titles'
    ];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'surname',
        'name',
        'phone',
        'persist_code',
        'is_enabled'
    ];

    /**
     * @var array Hidden fields
     */
    protected $hidden = ['email', 'gender', 'age', 'sms_code'];

    /**
     * @var array Relations
     */
    public $hasOne = [
        'qr_code' => ['Kozmo\Common\Models\QrCode', 'key' => 'user_id'],
        'shelf' => ['Kozmo\Common\Models\Shelf', 'key' => 'user_id'],
    ];
    public $hasMany = [
        'shelves' => ['Kozmo\Common\Models\Shelf', 'key' => 'user_id'],
        'orders' => ['Kozmo\Payment\Models\Order', 'key' => 'user_id'],
        'checkins' => ['Kozmo\Personal\Models\Checkin', 'key' => 'user_id'],
        'smscodes' => ['Kozmo\Personal\Models\SmsCodes', 'key' => 'phone', 'otherKey' => 'phone'],
        'subscriptions' => ['Kozmo\Personal\Models\Subscription', 'key' => 'user_id'],
    ];
    public $belongsTo = [];
    public $belongsToMany = [
        'rooms' => [
            'Kozmo\Sport\Models\Room',
            'table' => 'kozmo_sport_room_user',
            'key' => 'user_id',
            'otherKey' => 'room_id'
        ],
        'freshes' => [
            'Kozmo\Sport\Models\Fresh',
            'table' => 'kozmo_sport_fresh_user',
            'key' => 'user_id',
            'otherKey' => 'fresh_id'
        ]
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
    *   Scopes
    */
    public function scopeByPhone($query, $value)
    {
        return $query->where('phone', $value);
    }

    public function scopeEnabled($query)
    {
        return $query->where('is_enabled', 1);
    }

    public function scopeDisabled($query)
    {
        return $query->where('is_enabled', 0);
    }

    /**
    *   Events
    */
    public function beforeCreate()
    {
        $this->is_enabled = false;
    }

    public function afterCreate()
    {
      if (!$fresh = Fresh::published()->first()) {
        return;
      }
      $this->freshes()->detach();
      $this->freshes()->attach($fresh->id);
    }

    /**
    *   Attributes
    */
    public function getHasShelfAttribute($value)
    {
        $subs = $this->subscriptions()->enabled();
        foreach ($subs->lists('has_shelf') as $value) {
            if ($value) {
                return true;
            }
        }
        return false;
    }

    public function getHasSportswearAttribute($value)
    {
        $subs = $this->subscriptions()->enabled();
        foreach ($subs->lists('has_sportswear') as $value) {
            if ($value) {
                return true;
            }
        }
        return false;
    }

    public function getHasFreshAttribute($value)
    {
        $subs = $this->subscriptions()->enabled();
        foreach ($subs->lists('has_fresh') as $value) {
            if ($value) {
                return true;
            }
        }
        return false;
    }

    public function getHasScheduleAttribute($value)
    {
        $subs = $this->subscriptions()->enabled();
        foreach ($subs->lists('has_schedule') as $value) {
            if ($value) {
                return true;
            }
        }
        return false;
    }

    public function getHasFreezeAttribute($value)
    {
        $subs = $this->subscriptions()->enabled();
        foreach ($subs->lists('has_freeze') as $value) {
            if ($value) {
                return true;
            }
        }
        return false;
    }

    public function getIsFrozenAttribute($value)
    {
        $subs = $this->subscriptions()->enabled();
        foreach ($subs->lists('is_frozen') as $value) {
            if ($value) {
                return true;
            }
        }
        return false;
    }

    public function getHasSubscriptionAttribute($value)
    {
        if (!$this->subscriptions()->enabled()->get()) {
            return false;
        }
        $count = $this->subscriptions()->enabled()->count();
        return $count > 0;
    }

    public function getHasSubscriptionSport()
    {
        if (!$this->subscriptions()->enabled()->byProductSport()->get()) {
            return false;
        }
        $count = $this->subscriptions()->enabled()->byProductSport()->count();
        return $count > 0;
    }

    public function getHasSubscriptionYoga()
    {
        if (!$this->subscriptions()->enabled()->byProductYoga()->get()) {
            return false;
        }
        $count = $this->subscriptions()->enabled()->byProductYoga()->count();
        return $count > 0;
    }

    public function getSubscriptionDataAttribute($value)
    {
    //   return $this->subscriptions()->enabled()->get();
        $data = [];
        $data['product_title'] = '';
        $data['is_free'] = 0;
        $data['is_enabled'] = 0;
        $data['has_fresh'] = 0;
        $data['has_count'] = 0;
        $data['has_period'] = 0;
        $data['has_schedule'] = 0;
        $data['has_shelf'] = 0;
        $data['has_sportswear'] = 0;
        $data['has_freeze'] = 0;
        $data['is_frozen'] = 0;

        $data['counter_limits'] = '';
        $data['counter_titles'] = '';

        $limits = [];
        $limits['values'] = [];
        $limits['titles'] = [];

        $titles = [];

        $subs = $this->subscriptions()->enabled()->get();
        if ($subs->isEmpty()) {
            return [];
        }
        foreach ($subs as $sub) {
            $titles[] = $sub->product_title;
            $data['is_free'] = ($sub->is_free) ? 1 : $data['is_free'];
            $data['is_enabled'] = ($sub->is_enabled) ? 1 : $data['is_enabled'];
            $data['has_fresh'] = ($sub->has_fresh) ? 1 : $data['has_fresh'];
            $data['has_count'] = ($sub->has_count) ? 1 : $data['has_count'];
            $data['has_period'] = ($sub->has_period) ? 1 : $data['has_period'];
            $data['has_schedule'] = ($sub->has_schedule) ? 1 : $data['has_schedule'];
            $data['has_shelf'] = ($sub->has_shelf) ? 1 : $data['has_shelf'];
            $data['has_sportswear'] = ($sub->has_sportswear) ? 1 : $data['has_sportswear'];
            $data['has_freeze'] = ($sub->has_freeze) ? 1 : $data['has_freeze'];
            $data['is_frozen'] = ($sub->is_frozen) ? 1 : $data['is_frozen'];
            if ($sub->is_frozen) {
                $limits['values'][] = $sub->freeze_limit;
                $limits['titles'][] = $sub->freeze_title;
            } else {
                if ($sub->has_count) {
                    $limits['values'][] = $sub->visits_limit;
                    $limits['titles'][] = $sub->visits_title;
                }
                if ($sub->has_period) {
                    $limits['values'][] = $sub->period_limit;
                    $limits['titles'][] = $sub->period_title;
                }
            }
        }
        $data['product_title'] = implode(' / ', $titles);
        $data['counter_limits'] = implode(' / ', $limits['values']);
        $data['counter_titles'] = implode(' / ', $limits['titles']);

        return $data;
    }

    public function getFreshDataAttribute($value)
    {
      return $this->freshes()->published()->first();
    }

    public function getRoomsDataAttribute($value)
    {
      $data = $this->rooms()->published()->actual()->get()->groupBy(function($item, $key) {
          return $item['day'];
      });
      $roomsMap = [];
      $counter = 0;
      foreach ($data as $day => $rooms) {
        if ($counter >= self::LIMIT_ROOMS_DAYS) {
            break;
        }
        $counter = $counter + 1;
        $temp = ['day' => $day, 'data' => $rooms];
        $roomsMap[] = $temp;
      }
      return $roomsMap;
    }

    public function getQrCodeDataAttribute($value)
    {
        return $this->qr_code()->first();
    }

    public function getShelvesDataAttribute($value)
    {
        return $this->shelves()->get();
    }

    public function getSubscriptionLimits()
    {
        $limits = [];
        $limits['values'] = [];
        $limits['titles'] = [];

        $subs = $this->subscriptions()->enabled()->get();
        if ($subs->isEmpty()) {
            return $limits;
        }
        foreach ($subs as $sub) {
            if ($sub->is_frozen) {
                $limits['values'][] = $sub->freeze_limit;
                $limits['titles'][] = $sub->freeze_title;
            } else {
                if ($sub->has_count) {
                    $limits['values'][] = $sub->visits_limit;
                    $limits['titles'][] = $sub->visits_title;
                }
                if ($sub->has_period) {
                    $limits['values'][] = $sub->period_limit;
                    $limits['titles'][] = $sub->period_title;
                }
            }
        }
        return $limits;
    }

    public function getCounterLimitsAttribute($value)
    {
        $limits = $this->getSubscriptionLimits();
        if (empty($limits['values'])) {
            return '';
        }
        return implode(' / ', $limits['values']);
    }

    public function getCounterTitlesAttribute($value)
    {
        $limits = $this->getSubscriptionLimits();
        if (empty($limits['titles'])) {
            return '';
        }
        return implode(' / ', $limits['titles']);
    }

    public function getPhoneWithPrefix()
    {
        return str_replace('+7', '8', $this->phone);
    }
    /**
    *   Methods
    */
    public function checkRoomsDailyLimit($date = null)
    {
        if (!$date) {
            $date = Carbon::now();
        }
        $roomsCount = $this->rooms()->byDate($date)->count();
        return $roomsCount < self::LIMIT_ROOMS_DAILY;
    }

    public function setEnabled()
    {
        $this->is_enabled = true;
        return $this->save();
    }

    public function setDisabled()
    {
        $this->is_enabled = false;
        return $this->save();
    }

    public function checkSubscriptions($product)
    {
        if (env('APP_DISABLE_SUBS_CHECK')) {
            return true;
        }
        if ($product->is_free) {
            $count = $this->subscriptions()->free()->byProductId($product->id)->count();
            return $count == 0;
        } else {
            $count = $this->subscriptions()->byProductType($product->type)->enabled()->count();
            return $count == 0;
        }
        return false;
    }

    // public function reduceVisitsLimit()
    // {
    //     if ($subscription = $this->subscriptions()->free()->hasCount()->enabled()->first()) {
    //         $subscription->visits_limit -= 1;
    //         $subscription->save();
    //     }
    //     if ($subscription = $this->subscriptions()->hasCount()->enabled()->first()) {
    //         if (!$subscription->is_frozen) {
    //             $subscription->visits_limit -= 1;
    //             $subscription->save();
    //         }
    //     }
    // }

    public function setUnfreeze()
    {
        $subs = $this->subscriptions()->enabled()->frozen()->get();
		if ($subs->isEmpty()) {
			return;
		}
		foreach ($subs as $sub) {
			$sub->setUnfreeze();
		}
    }
}
