<?php namespace Kozmo\Personal\Models;

use Log;
use Model;
use Carbon\Carbon;

/**
 * Checkin Model
 */
class Checkin extends Model
{
    const STATUS_IN = 'in';
    const STATUS_OUT = 'out';
    /**
     * @var string The database table used by the model.
     */
    public $table = 'kozmo_personal_checkins';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Dates fields
     */
    protected $dates = ['checked_in_at', 'checked_out_at'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'room' => ['Kozmo\Sport\Models\Room', 'key' => 'room_id'],
        'user' => ['Kozmo\Personal\Models\User', 'key' => 'user_id'],
        'scanner' => ['Kozmo\Common\Models\Scanner', 'key' => 'scanner_id'],
        'subscription' => ['Kozmo\Personal\Models\Subscription', 'key' => 'subscription_id']
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
    *   Scopes
    */
    public function scopeByStatus($query, $value)
    {
        return $query->where('status', $value);
    }

    public function scopeIn($query)
    {
        return $query->where('status', self::STATUS_IN);
    }

    public function scopeOut($query)
    {
        return $query->where('status', self::STATUS_OUT);
    }

    public function scopeByCredentials($query, $scanner, $user)
    {
        return $query->where('scanner_id', $scanner)
            ->where('user_id', $user);
    }

    public function scopeByRoom($query, $room)
    {
        return $query->where('room_id', $room);
    }

    public function scopeToday($query)
    {
        $start = Carbon::now()->copy()->startOfDay();
        $end = Carbon::now()->copy()->endOfDay();
        return $query->where('checked_in_at', '>=', $start)->where('checked_in_at', '<', $end);
    }

    public function scopeHour($query)
    {
        $start = Carbon::now()->copy()->subHours(2);
        $end = Carbon::now();
        return $query->where('checked_in_at', '>=', $start)->where('checked_in_at', '<', $end);
    }

    /*
    *  Options
    */
    public function getStatusOptions()
    {
        return [
            self::STATUS_IN => 'Зашел',
            self::STATUS_OUT => 'Вышел'
        ];
    }

    /*
    *  Methods
    */
    public function setStatusIn()
    {
        $this->status = self::STATUS_IN;
        $this->checked_in_at = Carbon::now();
        $this->save();
    }

    public function setStatusOut()
    {
        $this->status = self::STATUS_OUT;
        $this->checked_out_at = Carbon::now();
        $this->save();
    }

    public function isStatusIn()
    {
        return $this->status == self::STATUS_IN;
    }

    public function isStatusOut()
    {
        return $this->status == self::STATUS_OUT;
    }

    public function getRoomPrice()
    {
        if (!$subs = $this->subscription) {
            if (!$subs = $this->user->subscriptions()->byProductYoga()->first()) {
                Log::info('ROOM PRICE: No subs at '.$this->id);
                return 0;
            }
        }
        if ($subs->is_free) {
            Log::info('ROOM PRICE: Subs is free at CHECKIN '.$this->id.' & SUBS '.$subs->id);
            return 0;
        }
        if (!$product = $subs->product) {
            $price = $subs->per_price;
            if (is_null($price)) {
                Log::info('ROOM PRICE: Subs price is NULL at CHECKIN '.$this->id.' & SUBS '.$subs->id);
            }
        } else {
            $price = $subs->product->per_price;
            if (is_null($price)) {
                Log::info('ROOM PRICE: Product price is NULL at CHECKIN '.$this->id.' & SUBS '.$subs->id.' & PRODUCT '.$product->id);
            }
        }
        $price = is_null($price)? 0: $price;
        //Log::info('ROOM PRICE: After null check price is '.$price.' at CHECKIN '.$this->id);
        return $price;
    }
}
