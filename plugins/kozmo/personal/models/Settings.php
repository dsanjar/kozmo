<?php namespace Kozmo\Personal\Models;

use October\Rain\Database\Model;

class Settings extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];
    public $settingsCode = 'kozmo_personal_settings';
    public $settingsFields = 'fields.yaml';

}
