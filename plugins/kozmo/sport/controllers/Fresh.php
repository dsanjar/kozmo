<?php namespace Kozmo\Sport\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Fresh Back-end Controller
 */
class Fresh extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Kozmo.Sport', 'sport', 'fresh');
    }
}
