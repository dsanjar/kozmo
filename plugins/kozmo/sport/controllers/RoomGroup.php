<?php namespace Kozmo\Sport\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Room Group Back-end Controller
 */
class RoomGroup extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Kozmo.Sport', 'sport', 'roomgroup');
    }
}
