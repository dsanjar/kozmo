<?php namespace Kozmo\Sport\Controllers;

use Redirect;
use BackendMenu;
use Kozmo\Sport\Models\Room as RoomModel;
use Backend\Classes\Controller;

/**
 * Room Back-end Controller
 */
class Room extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $relationConfig = 'config_relation.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Kozmo.Sport', 'sport', 'room');
    }

    private function getChecked()
    {
        $checked = post('checked');
        if (empty($checked)) {
            return;
        }
        if (!$rooms = RoomModel::whereIn('id', $checked)->get()) {
            return;
        }
        return $rooms;
    }

    private function getShift()
    {
        $shift = post('shift');
        return $shift;
    }

    public function onClose()
    {
        if (!$rooms = $this->getChecked()) {
            return;
        }
        foreach ($rooms as $room) {
            $room->setClose();
        }
        return;
    }

    public function onOpen()
    {
        if (!$rooms = $this->getChecked()) {
            return;
        }
        foreach ($rooms as $room) {
            $room->setOpen();
        }
        return;
    }

    public function onPublish()
    {
        if (!$rooms = $this->getChecked()) {
            return;
        }
        foreach ($rooms as $room) {
            $room->setPublish();
        }
        return;
    }

    public function onUnpublish()
    {
        if (!$rooms = $this->getChecked()) {
            return;
        }
        foreach ($rooms as $room) {
            $room->setUnpublish();
        }
        return;
    }

    public function onDuplicate()
    {
        if (!$rooms = $this->getChecked()) {
            return;
        }
        $shift = $this->getShift();

        foreach ($rooms as $room) {
            $newRoom = $room->replicate();
            $newRoom->started_at = $newRoom->started_at->addWeeks($shift);
            $newRoom->ended_at = $newRoom->ended_at->addWeeks($shift);
            $newRoom->likes_count = 0;
            $newRoom->save();
            if ($trainer = $room->trainers()->first()) {
                $newRoom->trainers()->attach($trainer->id);
            }
        }
        return Redirect::refresh();
    }
}
