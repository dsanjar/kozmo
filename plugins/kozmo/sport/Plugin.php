<?php namespace Kozmo\Sport;

use Backend;
use System\Classes\PluginBase;

/**
 * Sport Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Sport',
            'description' => 'No description provided yet...',
            'author'      => 'Kozmo',
            'icon'        => 'icon-heartbeat'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        $this->registerConsoleCommand('fake.schedule', 'Kozmo\Sport\Console\FakeSchedule');
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Kozmo\Sport\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'kozmo.sport.common' => [
                'tab' => 'Sport',
                'label' => 'Sport permission'
            ],
            'kozmo.sport.schedule' => [
                'tab' => 'Sport',
                'label' => 'Schedule permission'
            ],
            'kozmo.sport.api' => [
                'tab' => 'Sport',
                'label' => 'API data permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'sport' => [
                'label'       => 'Sport',
                'url'         => Backend::url('kozmo/sport/room'),
                'icon'        => 'icon-heartbeat',
                'permissions' => ['kozmo.sport.common'],
                'order'       => 400,
                'sideMenu' => [
                    'product' => [
                        'label' => 'Products',
                        'url'   => Backend::url('kozmo/sport/product'),
                        'icon'  => 'icon-circle-thin',
                        'permissions' => ['kozmo.sport.api'],
                        'order' => 10
                    ],
                    'freshes' => [
                        'label' => 'Freshes',
                        'url'   => Backend::url('kozmo/sport/fresh'),
                        'icon'  => 'icon-circle-thin',
                        'permissions' => ['kozmo.sport.api'],
                        'order' => 20
                    ],
                    'room' => [
                        'label' => 'Rooms',
                        'url'   => Backend::url('kozmo/sport/room'),
                        'icon'  => 'icon-circle-thin',
                        'permissions' => ['kozmo.sport.schedule'],
                        'order' => 30
                    ],
                    'trainer' => [
                        'label' => 'Trainers',
                        'url'   => Backend::url('kozmo/sport/trainer'),
                        'icon'  => 'icon-circle-thin',
                        'permissions' => ['kozmo.sport.schedule'],
                        'order' => 40
                    ],
                ]
            ],
        ];
    }

}
