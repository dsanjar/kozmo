<?php namespace Kozmo\Sport\Console;

use DB;
use Faker\Factory as Faker;
use Carbon\Carbon;
use ApplicationException;
use Kozmo\Sport\Models\Room;
use Kozmo\Sport\Models\Trainer;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class FakeSchedule extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'fake:schedule';

    /**
     * @var string The console command description.
     */
    protected $description = 'Сидим расписание.';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $faker = Faker::create();
        $trainers = Trainer::published()->get();
        $lastRoom = Room::published()->get()->last();
        if ($lastRoom && $lastRoom->ended_at->gte(Carbon::now())) {
            $lastDatetime = $lastRoom->ended_at;
        } else {
            $lastDatetime = Carbon::now();
        }

        for ($i=0; $i < 30; $i++) {
            $lastDatetime->addDay(1)->hour(7)->minute(30)->second(0);
            for ($j=0; $j < rand(5, 8); $j++) {
                $room = new Room;
                $room->likes_max = rand(10, 15);
                $room->likes_count = 0;
                $room->title = $faker->sentence(rand(2, 4));
                $room->summary = $faker->paragraph(rand(2, 4));
                $room->is_published = true;
                $room->started_at = $lastDatetime->addMinute(30);
                $room->ended_at = $lastDatetime->addMinute(rand(45, 90));
                $room->save();
                $room->trainers()->attach($trainers->random()->id);
            }
        }
    }
    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }

}
