<?php namespace Kozmo\Sport\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddFreezeFieldToProductsTable extends Migration
{
    public function up()
    {
        Schema::table('kozmo_sport_products', function(Blueprint $table) {
            $table->boolean('has_freeze')->default(0);
            $table->integer('freeze_limit')->nullable();
        });
    }

    public function down()
    {
        Schema::table('kozmo_sport_products', function(Blueprint $table) {
            $table->dropColumn('has_freeze');
            $table->dropColumn('freeze_limit');
        });
    }
}
