<?php namespace Kozmo\Sport\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddCheckinFieldsToRoomsTable extends Migration
{
    public function up()
    {
        Schema::table('kozmo_sport_rooms', function(Blueprint $table) {
            $table->boolean('is_closed')->default(0);
            $table->integer('room_group_id')->nullable();
        });
    }

    public function down()
    {
        Schema::table('kozmo_sport_rooms', function(Blueprint $table) {
            $table->dropColumn('is_closed');
            $table->dropColumn('room_group_id');
        });
    }
}
