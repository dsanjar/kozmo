<?php namespace Kozmo\Sport\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateTrainersTable extends Migration
{
    public function up()
    {
        Schema::create('kozmo_sport_trainers', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('fullname');
            $table->text('summary');
            $table->text('text');
            $table->string('image');
            $table->boolean('is_published')->default(0);
            $table->timestamps();
        });

        Schema::create('kozmo_sport_trainer_room', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('room_id')->unsigned();
            $table->integer('trainer_id')->unsigned();
            $table->primary(['room_id', 'trainer_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('kozmo_sport_trainers');
        Schema::dropIfExists('kozmo_sport_trainer_room');
    }
}
