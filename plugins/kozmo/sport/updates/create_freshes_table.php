<?php namespace Kozmo\Sport\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateFreshesTable extends Migration
{
    public function up()
    {
        Schema::create('kozmo_sport_freshes', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title');
            $table->text('summary');
            $table->text('text');
            $table->string('image');
            $table->boolean('is_published')->default(0);
            $table->integer('sort_order')->default(0);
            $table->timestamps();
        });

        Schema::create('kozmo_sport_fresh_user', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('user_id')->unsigned();
            $table->integer('fresh_id')->unsigned();
            $table->primary(['user_id', 'fresh_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('kozmo_sport_freshes');
        Schema::dropIfExists('kozmo_sport_fresh_user');
    }
}
