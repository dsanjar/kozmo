<?php namespace Kozmo\Sport\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddHasShelfFieldToProductsTable extends Migration
{
    public function up()
    {
        Schema::table('kozmo_sport_products', function(Blueprint $table) {
            $table->boolean('has_shelf')->default(0);
            $table->boolean('has_sportswear')->default(0);
        });
    }

    public function down()
    {
        Schema::table('kozmo_sport_products', function(Blueprint $table) {
            $table->dropColumn('has_shelf');
            $table->dropColumn('has_sportswear');
        });
    }
}
