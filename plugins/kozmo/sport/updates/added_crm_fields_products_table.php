<?php namespace Kozmo\Sport\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddCheckinFieldsToRoomsTable extends Migration
{
    public function up()
    {
        Schema::table('kozmo_sport_products', function(Blueprint $table) {
            $table->integer('crm_id')->nullable();
        });
    }

    public function down()
    {
        Schema::table('kozmo_sport_products', function(Blueprint $table) {
            $table->dropColumn('crm_id');
        });
    }
}
