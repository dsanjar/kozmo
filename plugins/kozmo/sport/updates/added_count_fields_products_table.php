<?php namespace Kozmo\Sport\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddCountFieldsToProductsTable extends Migration
{
    public function up()
    {
        Schema::table('kozmo_sport_products', function(Blueprint $table) {
            $table->integer('count')->default(0);
            $table->integer('period')->default(0);
        });
    }

    public function down()
    {
        Schema::table('kozmo_sport_products', function(Blueprint $table) {
            $table->dropColumn('count');
            $table->dropColumn('period');
        });
    }
}
