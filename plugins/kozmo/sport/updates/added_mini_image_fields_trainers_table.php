<?php namespace Kozmo\Sport\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddMiniImageFieldsToTrainersTable extends Migration
{
    public function up()
    {
        Schema::table('kozmo_sport_trainers', function(Blueprint $table) {
            $table->string('image_big')->nullable();
            $table->string('type')->nullable();
        });
    }

    public function down()
    {
        Schema::table('kozmo_sport_trainers', function(Blueprint $table) {
            $table->dropColumn('image_big');
            $table->dropColumn('type');
        });
    }
}
