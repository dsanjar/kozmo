<?php namespace Kozmo\Sport\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateRoomsTable extends Migration
{
    public function up()
    {
        Schema::create('kozmo_sport_rooms', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title');
            $table->text('summary');
            $table->boolean('is_published')->default(0);
            $table->integer('likes_count')->default(0);
            $table->integer('likes_max')->default(0);
            $table->timestamp('started_at')->nullable();
            $table->timestamp('ended_at')->nullable();
            $table->timestamps();
        });

        Schema::create('kozmo_sport_room_user', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('user_id')->unsigned();
            $table->integer('room_id')->unsigned();
            $table->primary(['user_id', 'room_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('kozmo_sport_rooms');
        Schema::dropIfExists('kozmo_sport_room_user');
    }
}
