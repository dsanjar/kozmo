<?php namespace Kozmo\Sport\Models;

use Model;
use Kozmo\Helpers\ImageHelper;

/**
 * Fresh Model
 */
class Fresh extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'kozmo_sport_freshes';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array The attributes that should be appends in arrays.
     */
    protected $appends = ['image_path', 'image_mini_path'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [
      'users' => [
          'Kozmo\Personal\Models\User',
          'table' => 'kozmo_sport_fresh_user',
          'key' => 'fresh_id',
          'otherKey' => 'user_id'
      ],
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
    *   Scopes
    */
    public function scopePublished($query)
    {
      return $query->where('is_published', 1);
    }

    /**
    *   Attributes
    */
    public function getImagePathAttribute($value)
    {
      if (!$this->image) {
        return;
      }
      return ImageHelper::publicUrl($this->image);
    }
    
    public function getImageMiniPathAttribute($value)
    {
      if (!$this->image_mini) {
        return;
      }
      return ImageHelper::publicUrl($this->image_mini);
    }
}
