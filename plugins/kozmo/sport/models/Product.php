<?php namespace Kozmo\Sport\Models;

use Model;
use Carbon\Carbon;
use Kozmo\Helpers\ImageHelper;

/**
 * Product Model
 */
class Product extends Model
{
    const TYPE_SPORT = 'sport';
    const TYPE_YOGA = 'yoga';

    const UNITS_VISITS = 'visits';
    const UNITS_DAYS = 'days';
    /**
     * @var string The database table used by the model.
     */
    public $table = 'kozmo_sport_products';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array The attributes that should be appends in arrays.
     */
    protected $appends = ['features_data', 'image_path', 'image_mini_path'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
      'features' => ['Kozmo\Sport\Models\Feature', 'key' => 'product_id']
    ];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
    *   Scopes
    */
    public function scopePublished($query)
    {
      return $query->where('is_published', 1);
    }

    public function scopeByFree($query)
    {
        return $query->orderBy('is_free', 'desc');
    }

    public function scopeByPrice($query)
    {
        return $query->orderBy('price', 'asc');
    }

    public function scopeByType($query)
    {
        return $query->orderBy('type', 'asc');
    }
    /**
    *   Attributes
    */
    public function getFeaturesDataAttribute($value)
    {
      return $this->features()->published()->get();
    }

    public function getImagePathAttribute($value)
    {
      if (!$this->image) {
        return;
      }
      return ImageHelper::publicUrl($this->image);
    }

    public function getImageMiniPathAttribute($value)
    {
      if (!$this->image_mini) {
        return;
      }
      return ImageHelper::publicUrl($this->image_mini);
    }

    public function getTypeOptions()
    {
        return [
            self::TYPE_YOGA => 'Kozmo Йога',
            self::TYPE_SPORT => 'Kozmo Спорт'
        ];
    }

    public function getVisitsLimit()
    {
        if ($this->has_count) {
            return $this->count;
        }
        return $this->has_count;
    }

    public function getPeriodLimit()
    {
        if ($this->has_period) {
            $now = Carbon::now();
            return $this->getExpiredDate()->diffInDays($now->startOfDay());
        }
        return $this->has_period;
    }

    public function getExpiredDate()
    {
        if (!$this->has_period) {
            return;
        }
        if (!$this->period) {
            return;
        }
        $now = Carbon::now();
        return $now->addMonths($this->period)->endOfDay();
    }

}
