<?php namespace Kozmo\Sport\Models;

use Model;
use Carbon\Carbon;

/**
 * Room Model
 */
class Room extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'kozmo_sport_rooms';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Dates fields
     */
    protected $dates = ['started_at', 'ended_at'];

    /**
     * @var array The attributes that should be appends in arrays.
     */
    protected $appends = ['day', 'already_started', 'is_full', 'is_block', 'trainer_data'];

    /**
     * @var array Hidden fields
     */
    protected $hidden = ['trainers', 'users'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'checkins' => ['Kozmo\Personal\Models\Checkin', 'key' => 'room_id']
    ];
    public $belongsTo = [];
    public $belongsToMany = [
        'trainers' => [
            'Kozmo\Sport\Models\Trainer',
            'table' => 'kozmo_sport_trainer_room',
            'key' => 'room_id',
            'otherKey' => 'trainer_id'
        ],
        'users' => [
            'Kozmo\Personal\Models\User',
            'table' => 'kozmo_sport_room_user',
            'key' => 'room_id',
            'otherKey' => 'user_id'
        ]
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
    *   Scopes
    */
    public function scopePublished($query)
    {
      return $query->where('is_published', 1)->orderBy('started_at', 'asc');
    }

    public function scopeClosed($query)
    {
        return $query->where('is_closed', 1);
    }

    public function scopeNotClosed($query)
    {
        return $query->where('is_closed', 0);
    }

    public function scopeActual($query)
    {
        // return $query;
      return $query->where('started_at', '>=', Carbon::now()->startOfDay());
    }

    public function scopeByDate($query, $date)
    {
        return $query
            ->where('started_at', '>=', $date->copy()->startOfDay())
            ->where('ended_at', '<=', $date->copy()->endOfDay());
    }

    public function scopeByDatePeriod($query, $start, $end)
    {
        return $query
            ->where('started_at', '>=', $start)
            ->where('ended_at', '<=', $end);
    }

    public function scopeClosest($query)
    {
        return $query->where('is_published', 1)
            ->where('started_at', '>=', Carbon::now()->subMinutes(21))
            ->orderBy('started_at', 'asc');
    }
    /**
    *   Attributes
    */
    public function getTrainerDataAttribute($value)
    {
      return $this->trainers()->published()->first();
    }

    public function getAlreadyStartedAttribute($value)
    {
      $now = Carbon::now();
      return $this->started_at->lte($now);
    }

    public function getIsFullAttribute($value)
    {
      return $this->likes_count >= $this->likes_max;
    }

    public function getIsBlockAttribute($value)
    {
      return ($this->already_started && $this->is_full);
    }

    public function getDayAttribute($value)
    {
      return $this->started_at->format('Y-m-d');
    }

    /**
    *   Methods
    */
    public function increaseLikes($count = 1)
    {
        $this->likes_count = $this->likes_count + $count;
        return $this->save();
    }

    public function decreaseLikes($count = 1)
    {
        $this->likes_count = (($this->likes_count - $count) < 0)? 0: ($this->likes_count - $count);
        return $this->save();
    }

    public function setClose()
    {
        $this->is_closed = true;
        $this->save();
    }

    public function setOpen()
    {
        $this->is_closed = false;
        $this->save();
    }

    public function setPublish()
    {
        $this->is_published = true;
        $this->save();
    }

    public function setUnpublish()
    {
        $this->is_published = false;
        $this->save();
    }
}
