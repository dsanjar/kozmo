<?php namespace Kozmo\Sport\Models;

use Model;
use Kozmo\Helpers\ImageHelper;

/**
 * Trainer Model
 */
class Trainer extends Model
{
    const TYPE_YOGA = 'yoga';
    const TYPE_SPORT = 'sport';
    const TYPE_MASTER = 'master';

    /**
     * @var string The database table used by the model.
     */
    public $table = 'kozmo_sport_trainers';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array The attributes that should be appends in arrays.
     */
    protected $appends = ['image_path', 'image_big_path'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [
        'rooms' => [
            'Kozmo\Sport\Models\Room',
            'table' => 'kozmo_sport_trainer_room',
            'key' => 'trainer_id',
            'otherKey' => 'room_id'
        ],
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
    *   Scopes
    */
    public function scopePublished($query)
    {
      return $query->where('is_published', 1);
    }

    public function scopeByType($query, $value)
    {
        return $query->where('type', $value);
    }
    /**
    *   Attributes
    */
    public function getImagePathAttribute($value)
    {
      if (!$this->image) {
        return;
      }
      return ImageHelper::publicUrl($this->image);
    }

    public function getImageBigPathAttribute($value)
    {
      if (!$this->image_big) {
        return;
      }
      return ImageHelper::publicUrl($this->image_big);
    }

    /**
    *   Options
    */
    public function getTypeOptions()
    {
        return [
            self::TYPE_YOGA => 'Йога',
            self::TYPE_SPORT => 'Спорт',
            self::TYPE_MASTER => 'Мастер',
        ];
    }
}
