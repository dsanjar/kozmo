<?php namespace Kozmo\Sport\Models;

use Model;

/**
 * Feature Model
 */
class Feature extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'kozmo_sport_features';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
      'product' => ['Kozmo\Sport\Models\Product', 'key' => 'product_id']
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
    *   Scopes
    */
    public function scopePublished($query)
    {
      return $query->where('is_published', 1);
    }
}
