<?php namespace Kozmo\Payment\Models;

use Model;
use Kozmo\Helpers\PaymentHelper;

/**
 * Order Model
 */
class Order extends Model
{
    const STATUS_FAIL = 'fail';
    const STATUS_ACCEPT = 'accept';
    const STATUS_WAITING = 'waiting';

    /**
     * @var string The database table used by the model.
     */
    public $table = 'kozmo_payment_orders';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Hidden fields
     */
    protected $hidden = ['user_id', 'product_model'];


    /**
     * @var array Appends fields
     */
    protected $appends = ['currency_title', 'status_title'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'user' => ['Kozmo\Personal\Models\User', 'key' => 'user_id']
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
    *   Scopes
    */
    public function scopeWaiting($query)
    {
      return $query->where('status', self::STATUS_WAITING);
    }

    public function scopeAccept($query)
    {
      return $query->where('status', self::STATUS_ACCEPT);
    }

    public function scopeFail($query)
    {
      return $query->where('status', self::STATUS_FAIL);
    }

    public function scopeByKeys($query, $userId, $productId, $productType)
    {
        return $query->where('user_id', $userId)
                    ->where('product_id', $productId)
                    ->where('product_type', $productType);
    }
    /**
    *   Events
    */
    public function beforeCreate()
    {
        $this->status = self::STATUS_WAITING;
        if (!$product = $this->product) {
            return;
        }
        $this->product_title = $product->title;
        $this->product_price = $product->price;
    }

    /**
    *   Methods
    */
    public function getStatusOptions()
    {
        return [
            self::STATUS_FAIL => 'Отказан',
            self::STATUS_ACCEPT => 'Завершен',
            self::STATUS_WAITING => 'Ожидание'
        ];
    }

    public function getCurrencyOptions()
    {
        return PaymentHelper::getCurrencies();
    }

    public function setAccept()
    {
        $this->status = self::STATUS_ACCEPT;
        return $this->save();
    }

    public function setFail()
    {
        $this->status = self::STATUS_FAIL;
        return $this->save();
    }

    /**
    *   Attributes
    */
    public function getProductAttribute($value)
    {
        if(!$this->product_model || !$this->product_id) {
            return null;
        }
        if (!class_exists($this->product_model)) {
            return null;
        }
        if (!$product = call_user_func_array($this->product_model.'::find', [$this->product_id])) {
            return null;
        }
        return $product;
    }

    public function getCurrencyTitleAttribute($value)
    {
        if (!$currency = $this->currency_id) {
            return;
        }
        $titles = PaymentHelper::getCurrenciesTitles();
        return $titles[$currency];
    }

    public function getStatusTitleAttribute($value)
    {
        if (!$status = $this->status) {
            return;
        }
        $options = $this->getStatusOptions();
        return $options[$status];
    }
}
