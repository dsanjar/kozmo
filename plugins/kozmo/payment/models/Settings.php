<?php namespace Kozmo\Payment\Models;

use October\Rain\Database\Model;
use Kozmo\Helpers\PaymentHelper;

class Settings extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];
    public $settingsCode = 'kozmo_payment_settings';
    public $settingsFields = 'fields.yaml';

    public function getDefaultCurrencyOptions($value, $formData)
    {
        return PaymentHelper::getCurrencies();
    }
}
