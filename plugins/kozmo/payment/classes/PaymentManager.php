<?php namespace Kozmo\Payment\Classes;

use Log;
use Event;
use Kozmo\Helpers\CurlHelper;
use Kozmo\Helpers\PaymentHelper;
use Kozmo\Common\Models\Shelf;
use Kozmo\Common\Models\QrCode;
use Kozmo\Personal\Models\User;
use Kozmo\Personal\Models\Subscription;
use Kozmo\Payment\Models\Order;
use Kozmo\Payment\Models\Invoice;
use Kozmo\Payment\Models\Settings;

class PaymentManager
{
	use \October\Rain\Support\Traits\Singleton;

	private $failUrl;
	private $walletUrl;
	private $successUrl;
	private $merchantId;
	private $merchantKey;
	private $defaultCurrency;

	/**
	*  Init settings
	*/
	public function init()
	{
		$this->failUrl 			= Settings::get('fail_url');
		$this->successUrl 		= Settings::get('success_url');
		$this->walletUrl 		= Settings::get('wallet_url');
		$this->merchantId 		= Settings::get('merchant_id');
		$this->merchantKey 		= Settings::get('merchant_key');
		$this->defaultCurrency 	= Settings::get('default_currency');
	}

	/**
	*  Checkout return redirect url for purchase on W1
	*/
	public function checkout($user)
	{
		if (!$agent = post('agent')) {
			return;
		}
		if (!PaymentHelper::checkAgent($agent)) {
			return;
		}
		if (!$productId = post('product_id')) {
			return;
		}
		if (!$productType = post('product_type')) {
			return;
		}
		if (!$product = PaymentHelper::findModelByType($productId, $productType)) {
			return;
		}
		if (!$user->checkSubscriptions($product)) {
			return;
		}
		if (!$productModel = PaymentHelper::getModelByType($productType)) {
			return;
		}
		if (!$order = Order::byKeys($user->id, $productId, $productType)->waiting()->first()) {
			$order = new Order;
		}
		$order->user_id = $user->id;
		$order->product_id = $productId;
		$order->product_type = $productType;
		$order->product_model = $productModel;
		$order->currency_id = $this->defaultCurrency;
		$order->save();

		if (!$user->checkSubscriptions($order->product)) {
			return;
		}

		if ($order->product->is_free) {
			$order->setAccept();
			$this->assign($user, $order);
			$result = [];
			$result['action'] = 'skip';
			if (PaymentHelper::isAgentWeb($agent)) {
				$result['redirect_url'] = Settings::get('custom_success_url');
			}
			return $result;
		}
		$params = [
			'WMI_MERCHANT_ID'	 => $this->merchantId,
			'WMI_PAYMENT_NO'	 => $order->id,
			'WMI_PAYMENT_AMOUNT' =>	$order->product_price,
			'WMI_CURRENCY_ID'	 => $order->currency_id,
			'WMI_DESCRIPTION'	 =>	"BASE64:".base64_encode($order->product_title),
			'WMI_SUCCESS_URL'	 => $this->successUrl.'/'.$agent,
			'WMI_FAIL_URL'		 => $this->failUrl.'/'.$agent,
			'KOZMO_USER_ID'		 => $user->id,
			'KOZMO_AGENT'		 => $agent
		];
		Log::info('Before payment '.json_encode($params));
		$curl = new CurlHelper($this->walletUrl);
		$response = $curl->post(
			[],
			$this->sign($params),
			['Content-Type'  => 'application/x-www-form-urlencoded'],
			true
		);
		if (!$response) {
			return;
		}
		if (!isset($response['redirect_url'])) {
			return;
		}
		return ['action' => 'purchase', 'redirect_url' => $response['redirect_url']];
	}

	/**
	*  Success callback
	*/
	public function callback()
	{
		$params = post();

		foreach ($params as $key => $value) {
			$params[$key] = urldecode($value);
		}

		if (!isset($params['WMI_PAYMENT_NO'])) {
			return;
		}
		if (!isset($params['WMI_ORDER_STATE'])) {
			return;
		}
		if (!$check = $this->checkSign($params)) {
			return;
		}
		if (!isset($params['KOZMO_USER_ID'])) {
			return;
		}
		if (!$user = User::find($params['KOZMO_USER_ID'])) {
			return;
		}
		if (!$order = Order::where('id', $params['WMI_PAYMENT_NO'])->waiting()->first()) {
			return;
		}

		$invoice 				= new Invoice;
		$invoice->order_id		= $params['WMI_PAYMENT_NO'];
		$invoice->w_order_id	= $params['WMI_ORDER_ID'];
		$invoice->w_to_user_id	= (isset($params['WMI_TO_USER_ID']))?$params['WMI_TO_USER_ID']:null;
		$invoice->w_currency_id	= $params['WMI_CURRENCY_ID'];
		$invoice->w_amount		= $params['WMI_PAYMENT_AMOUNT'];
		$invoice->w_commission	= $params['WMI_COMMISSION_AMOUNT'];
		$invoice->w_expired_at	= $params['WMI_EXPIRED_DATE'];
		$invoice->reference		= json_encode($params, JSON_UNESCAPED_UNICODE);
		$invoice->save();

		if (strtoupper($params['WMI_ORDER_STATE']) == 'ACCEPTED') {
			$order->setAccept();
			$invoice->setAccept();
			$this->assign($user, $order);
			return true;
		}
		$order->setFail();
		$invoice->setFail();
		return;
	}

	/**
	*  Assign subs and shelves
	*/
	public function assign($user, $order, $fireEvent = true)
	{
		$user->setEnabled();

		$subs 				 = new Subscription;
		$subs->user_id 		 = $user->id;
		$subs->product_id 	 = $order->product_id;
		$subs->product_type  = $order->product_type;
		$subs->product_model = $order->product_model;
		$subs->is_enabled	 = true;
		$subs->save();

		if ($subs->has_shelf) {
			if ($shelf = Shelf::enabled()->vacant()->byPriority()->first()) {
				$shelf->user_id = $user->id;
				$shelf->save();
			}
		}
		if (!$user->qr_code) {
			$query = ($subs->has_shelf)? QrCode::vacant()->printable(): QrCode::vacant()->notPrintable();
			if ($qrcode = $query->first()) {
				$qrcode->user_id = $user->id;
				$qrcode->save();
				$user->persist_code = $qrcode->unique_string;
				$user->save();
			}
		}
		if ($fireEvent) {
			Event::fire('crm.deal.add', [$user, $order, $subs]);
		}
	}

	/**
	*  Signature
	*/
	private function sign($params)
	{
		uksort($params, 'strcasecmp');

		$fieldValues = '';
		foreach($params as $value)
		{
			if(is_array($value)) {
				foreach($value as $v)
				{
					$v = iconv("utf-8", "windows-1251", $v);
					$fieldValues .= $v;
				}
			} else {
				$value = iconv("utf-8", "windows-1251", $value);
				$fieldValues .= $value;
			}
		}

		$params['WMI_SIGNATURE'] = base64_encode(pack("H*", md5($fieldValues . $this->merchantKey)));
		// $params['WMI_TEST_MODE_INVOICE'] = true;
		return $params;
	}

	private function checkSign($params)
	{
		if (env('APP_DISABLE_SIGNATURE')) {
			return true;
		}
		if (!isset($params['WMI_SIGNATURE'])) {
			return;
		}
		$signature = $params['WMI_SIGNATURE'];
		// unset($params['WMI_SIGNATURE']);
		$data = [];
		foreach($params as $name => $value)
		{
			if ($name !== "WMI_SIGNATURE") $data[$name] = $value;
		}

		uksort($data, 'strcasecmp');

		$fieldValues = '';
		foreach($data as $name => $value)
		{
			$fieldValues .= $value;
		}

		$check = base64_encode(pack("H*", md5($fieldValues . $this->merchantKey)));
		Log::info($check.' == '.$signature);
		return ($signature == $check);
	}
}
