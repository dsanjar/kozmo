<?php namespace Kozmo\Payment\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Invoice Back-end Controller
 */
class Invoice extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Kozmo.Payment', 'payment', 'invoice');
    }
}
