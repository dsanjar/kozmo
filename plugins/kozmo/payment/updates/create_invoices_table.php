<?php namespace Kozmo\Payment\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateInvoicesTable extends Migration
{
    public function up()
    {
        Schema::create('kozmo_payment_invoices', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('order_id')->nullable();
            $table->string('status')->nullable();

            $table->integer('w_order_id')->nullable();
            $table->string('w_to_user_id')->nullable();
            $table->integer('w_currency_id')->nullable();
            $table->integer('w_amount')->nullable();
            $table->integer('w_commission')->nullable();
            $table->timestamp('w_expired_at')->nullalbe();
            $table->text('reference')->nullable();
            // $table->boolean('w_is_test')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('kozmo_payment_invoices');
    }
}
