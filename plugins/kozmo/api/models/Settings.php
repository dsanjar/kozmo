<?php namespace Kozmo\Api\Models;

use October\Rain\Database\Model;
use Kozmo\Sport\Models\Product;

class Settings extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];
    public $settingsCode = 'kozmo_api_settings';
    public $settingsFields = 'fields.yaml';

    public function getSportProductsOptions($value, $formData)
    {
        $products = Product::published()->where('is_free', 1)->lists('title', 'id');
        $options = ['' => 'Nothing selected'];
        $options += $products;
        return $options;
    }
}
