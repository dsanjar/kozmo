<?php namespace Kozmo\Api;

use Backend;
use System\Classes\PluginBase;

/**
 * Api Plugin Information File
 */
class Plugin2 extends PluginBase
{
  /**
   * Returns information about this plugin.
   *
   * @return array
   */
  public function pluginDetails()
  {
      return [
          'name'        => 'Api',
          'description' => 'API for apps',
          'author'      => 'Kozmo',
          'icon'        => 'icon-plug'
      ];
  }

  public function registerSettings()
  {
      return [
          'settings' => [
              'category'    => 'Api',
              'label'       => 'Settings',
              'description' => 'Manage api settings.',
              'icon'        => 'icon-plug',
              'class'       => 'Kozmo\Api\Models\Settings',
              'order'       => 500,
              'keywords'    => 'api settings',
              'permissions' => ['kozmo.api.*']
          ]
      ];
  }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Kozmo\Api\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'kozmo.api.some_permission' => [
                'tab' => 'Api',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'api' => [
                'label'       => 'Api',
                'url'         => Backend::url('kozmo/api/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['kozmo.api.*'],
                'order'       => 500,
            ],
        ];
    }
}
