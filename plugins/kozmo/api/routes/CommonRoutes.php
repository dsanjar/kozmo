<?php namespace Kozmo\Api\Routes;

use Argon\Argon;
use Kozmo\Common\Models\Splash;
use Kozmo\Common\Models\Contact;
use Kozmo\Api\Classes\ResponseManager;

class CommonRoutes
{
		public static function splashes() {
				$data = Splash::published()->get();
				return ResponseManager::success($data);
		}

		public static function splashesLast() {
				$data = Splash::published()->latest()->first();
				return ResponseManager::success($data);
		}

		public static function contacts() {
				$data = Contact::published()->get();
				return ResponseManager::success($data);
		}
}
