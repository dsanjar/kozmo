<?php namespace Kozmo\Api\Routes;

use Log;
use Event;
use Kozmo\Personal\Models\User;
use Kozmo\Payment\Models\Order;
use Kozmo\Helpers\PaymentHelper;
use Kozmo\Payment\Models\Settings;
use Kozmo\Api\Classes\CurlManager;
use Kozmo\Api\Classes\AuthManager;
use Kozmo\Api\Classes\ResponseManager;
use Kozmo\Payment\Classes\PaymentManager;
use Kozmo\Sport\Models\Product as SportProduct;

class CRMRoutes
{
	public static function dealAdd() {
		$user = User::find(6);
		$subs = $user->subscriptions()->enabled()->first();
		if (!$subs) {
			return ResponseManager::error('defaul');
		}
		// Event::fire('crm.deal.add', [$user, $subs]);
		return ResponseManager::success();
	}

	public static function dealGet($id) {
		$curl = CurlManager::instance();
		$response = $curl->post(
			['crm.deal.get'],
			['id' => $id]
		);
		return ResponseManager::success($response);
	}

	public static function contactList($phone) {
		$curl = CurlManager::instance();

		$response = $curl->post(
			['crm.duplicate.findbycomm'],
			[
				'entity_type' => 'CONTACT',
				'type' => 'PHONE',
				'values' => [$phone, str_replace('+7', '8', $phone)]
			]
		);
		// $response = $curl->post(
		// 	['crm.contact.list'],
		// 	[
		// 		'filter' => ['PHONE' => $phone],
		// 		'select' => ['ID']
		// 	]
		// );
		return ResponseManager::success($response);
	}

	public static function dealUpdated() {
		$data = post();

		if (!isset($data['data']) || !isset($data['auth'])) {
			Log::info('Empty data in deal updated webhook');
			return ResponseManager::error('default');
		}
		if (!isset($data['auth']['application_token'])) {
			Log::info('Empty application token in deal updated webhook');
			return ResponseManager::error('default');
		}
		if ($data['auth']['application_token'] != config('crm.key')) {
			Log::info('Wrong application token in deal updated webhook');
			return ResponseManager::error('default');
		}
		if (!isset($data['data']['FIELDS'])) {
			Log::info('Empty fields in deal updated webhook');
			return ResponseManager::error('default');
		}
		if (empty($data['data']['FIELDS'])) {
			Log::info('Empty fields in deal updated webhook');
			return ResponseManager::error('default');
		}
		if (!isset($data['data']['FIELDS']['ID'])) {
			Log::info('Empty ID in deal updated webhook');
			return ResponseManager::error('default');
		}
		$deal = $data['data']['FIELDS']['ID'];

		if (!$dealData = self::getDeal($deal)) {
			Log::info('Get deal error');
			return ResponseManager::error('default');
		}
		if (!self::checkOrder($dealData['deal'])) {
			Log::info('Deal handled '.$dealData['deal']);
			return ResponseManager::error('default');
		}
		if (!$user = self::getContact($dealData['contact'])) {
			Log::info('Get contact error');
			return ResponseManager::error('default');
		}
		if (!$product = self::getProducts($deal)) {
			Log::info('Get product error');
			return ResponseManager::error('default');
		}
		self::assign($user, $product, $dealData['deal']);
		return ResponseManager::success();
	}

	public static function getDeal($deal)
	{
		$curl = CurlManager::instance();
		$response = $curl->post(
			['crm.deal.get'],
			['id' => $deal]
		);
		Log::info($response);

		if (!isset($response['result'])) {
			Log::info('Empty deal in deal updated webhook');
			return false;
		}

		if ($response['result']['STAGE_ID'] != 'WON') {
			Log::info('Wrong stage in deal updated webhook');
			return false;
		}
		if ($response['result']['TYPE_ID'] != 'COMPLEX') {
			Log::info('Wrong type in deal updated webhook');
			return false;
		}
		if (!isset($response['result']['CONTACT_ID'])) {
			Log::info('Empty contact in deal updated webhook');
			return false;
		}
		return ['deal' => $response['result']['ID'], 'contact' => $response['result']['CONTACT_ID']];
	}

	public static function getContact($contact)
	{
		$curl = CurlManager::instance();
		$response = $curl->post(
			['crm.contact.get'],
			['id' => $contact]
		);
		Log::info($response);

		if (!isset($response['result'])) {
			Log::info('Empty contacts in deal updated webhook');
			return ResponseManager::error('default');
		}
		if (empty($response['result'])) {
			Log::info('Empty contacts in deal updated webhook');
			return false;
		}
		if (!$user = self::signup($response['result'])) {
			Log::info('Signup error in deal updated webhook');
			return false;
		}
		return $user;
	}

	public static function getProducts($deal)
	{
		$curl = CurlManager::instance();
		$response = $curl->post(
			['crm.deal.productrows.get'],
			['id' => $deal]
		);
		Log::info($response);

		if (!isset($response['result'])) {
			Log::info('Empty products in deal updated webhook');
			return false;
		}
		if (empty($response['result'])) {
			Log::info('Empty products in deal updated webhook');
			return false;
		}
		foreach ($response['result'] as $result) {
			if (!isset($result['PRODUCT_ID'])) {
				continue;
			}
			if (!$product = SportProduct::/*published()->*/where('crm_id', $result['PRODUCT_ID'])->first()) {
				continue;
			}
			return $product;
		}
		Log::info('Not found products in deal updated webhook');
		return false;
	}

	public static function signup($data)
	{
		$authManager = AuthManager::instance();

		$userData = [];
		if (isset($data['NAME'])) {
			$userData['name'] = $data['NAME'];
		}
		if (isset($data['LAST_NAME'])) {
			$userData['surname'] = $data['LAST_NAME'];
		}
		if (isset($data['LAST_NAME'])) {
			$userData['surname'] = $data['LAST_NAME'];
		}
		if (isset($data['PHONE']) && !empty($data['PHONE'])) {
			$phone = trim(str_replace(' ', '', $data['PHONE'][0]['VALUE']));
			$phone = preg_replace('/^8/', '+7', $phone);
			$phone = str_replace('(', '', $phone);
			$phone = str_replace(')', '', $phone);
			$phone = str_replace('-', '', $phone);
			$userData['phone'] = $phone;
		}
		if (!$authManager->phoneValidate($phone)) {
			Log::info('Phone validation error in deal updated webhook');
			return false;
		}

		if (!$user = User::byPhone($phone)->enabled()->first()) {
			$result = $authManager->signUp(true, $userData);
			if (!is_array($result) || !isset($result['user'])) {
				Log::info('Authmanager signup error in deal updated webhook');
				return false;
			}
			$user = $result['user'];
		}
		return $user;
	}

	public static function assign($user, $product, $dealId)
	{
		if (!$agent = 'crm') {
			return;
		}
		if (!PaymentHelper::checkAgent($agent)) {
			Log::info('Check agent error.');
			return false;
		}
		if (!$user->checkSubscriptions($product)) {
			Log::info('Check subs error.');
			return false;
		}
		if (!$productModel = PaymentHelper::getModelByType($product->type)) {
			Log::info('Empty product model.');
			return false;
		}
		if (!$order = Order::where('crm_id', $dealId)->waiting()->first()) {
			$order = new Order;
		}
		$order->user_id = $user->id;
		$order->product_id = $product->id;
		$order->product_type = $product->type;
		$order->product_model = $productModel;
		$order->crm_id = $dealId;
		$order->currency_id = Settings::get('default_currency');
		$order->save();
		if (!$user->checkSubscriptions($product)) {
			Log::info('Double check subs error.');
			return false;
		}

		$paymentManager = PaymentManager::instance();
		$paymentManager->assign($user, $order, false);

		$order->setAccept();
		Log::info('Order '.$order->id.' crm_id '.$order->crm_id);

		return true;
	}

	public static function checkOrder($dealId)
	{
		if ($order = Order::where('crm_id', $dealId)->accept()->first()) {
			Log::info('Found order '.$order->id.' with crm_id '.$order->crm_id);
			return false;
		}
		Log::info('Not found order with crm_id'.$dealId);
		return true;
	}
}
