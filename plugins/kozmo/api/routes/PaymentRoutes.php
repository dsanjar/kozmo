<?php namespace Kozmo\Api\Routes;

use Log;
use Redirect;
use Argon\Argon;
use Kozmo\Helpers\PaymentHelper;
use Kozmo\Api\Classes\ResponseManager;
use Kozmo\Payment\Classes\PaymentManager;
use Kozmo\Payment\Models\Settings as PaymentSettings;

class PaymentRoutes
{
	public static function checkout($user = null) {
		$user = (!$user)? request('user'): $user;

		$checkout = PaymentManager::instance()->checkout($user);
		if (!$checkout) {
			return ResponseManager::error('checkout');
		}
		return ResponseManager::success($checkout);
	}

	public static function success($agent) {
		if (PaymentHelper::isAgentMobile($agent)) {
			return ResponseManager::view('kozmo.payment::success', ['agent' => $agent]);

		} else {
			return Redirect::to(PaymentSettings::get('custom_success_url'));
		}
	}

	public static function fail($agent) {
		if (PaymentHelper::isAgentMobile($agent)) {
			return ResponseManager::view('kozmo.payment::fail', ['agent' => $agent]);
		} else {
			return Redirect::to(PaymentSettings::get('custom_fail_url'));
		}
	}

	public static function callback() {
		$params = post();
		foreach ($params as $key => $value) {
			$params[$key] = urldecode($value);
		}
		Log::info(json_encode($params));

		if ($callback = PaymentManager::instance()->callback()) {
			return 'WMI_RESULT=OK';
		}
		return 'WMI_RESULT=RETRY&WMI_DESCRIPTION='.urlencode('ERROR');
	}
}
