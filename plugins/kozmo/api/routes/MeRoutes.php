<?php namespace Kozmo\Api\Routes;

use Event;
use Argon\Argon;
use Kozmo\Sport\Models\Room;
use Kozmo\Sport\Models\Fresh;
use Kozmo\Api\Classes\ResponseManager;

class MeRoutes
{
	public static function me() {
		return ResponseManager::success(request('user'));
	}

	public static function meCheck() {
		return ResponseManager::success(['authorized' => true, 'payload' => post()]);
	}

	public static function meFresh() {
		$user = request('user');
		if (!$fresh = $user->freshData) {
			ResponseManager::success();
		}
		return ResponseManager::success($fresh);
	}

	public static function meAddFresh($id) {
		if (!$fresh = Fresh::published()->where('id', $id)->first()) {
			return ResponseManager::error('default');
		}
		$user = request('user');
		$user->freshes()->detach();
		$user->freshes()->attach($fresh->id);

		return ResponseManager::success($fresh);
	}

	public static function meRooms() {
		$user = request('user');
		if (!$rooms = $user->rooms) {
			ResponseManager::success();
		}
		return ResponseManager::success($rooms);
	}

	public static function meRoomsMap() {
		$user = request('user');
		if (!$rooms = $user->roomsData) {
			ResponseManager::success();
		}
		return ResponseManager::success($rooms);
	}

	public static function meToggleRoom($id) {
		if (!$room = Room::published()->actual()->where('id', $id)->first()) {
			return ResponseManager::error('default');
		}
		$user = request('user');
		if ($has = $user->rooms()->where('id', $room->id)->first()) {
			$user->rooms()->detach($room->id);
			$room->decreaseLikes();
			Event::fire('room.out', [$room, $user]);
			return ResponseManager::success(['result' => 'removed', 'likes' => $room->likes_count]);
		} else {
			if (!$user->checkRoomsDailyLimit($room->started_at)) {
				return ResponseManager::error('rooms_daily_limit');
			}
			$user->rooms()->attach($room->id);
			$room->increaseLikes();
			Event::fire('room.in', [$room, $user]);
			return ResponseManager::success(['result' => 'added', 'likes' => $room->likes_count]);
		}
	}

	public static function meSubs() {
		$user = request('user');
		if (!$subscriptions = $user->subscriptions) {
			return ResponseManager::error('default');
		}
		return ResponseManager::success($subscriptions);
	}

	public static function meOrders() {
		$user = request('user');
		if (!$orders = $user->orders) {
			return ResponseManager::error('default');
		}
		return ResponseManager::success($orders);
	}

	public static function meFreeze() {
		$user = request('user');
		$subs = $user->subscriptions()->enabled()->hasFreeze()->get();
		if ($subs->isEmpty()) {
			return ResponseManager::error('user-no-freeze');
		}
		foreach ($subs as $sub) {
			$sub->setFreeze();
		}
		return ResponseManager::success();
	}
}
