<?php namespace Kozmo\Api\Classes;

use Log;
use ApplicationException;
use October\Rain\Extension\ExtensionBase;

class CurlManager extends ExtensionBase
{
    use \October\Rain\Support\Traits\Singleton;

    public function headers($headers = [])
    {
        $flat = [];
        if (!empty($headers)) {
            foreach ($headers as $key => $value) {
                $flat[] = $key.': '.$value;
            }
        }
        return $flat;
    }

    public function params($params = [])
    {
        $params = $this->sign($params);
        $params = http_build_query($params);
        return $params;
    }

    public function sign($params = [])
    {
        return $params;
    }

    public function url($routes = [], $url = null)
    {
        if (!$url) {
            $url = config('crm.url');
        }
        if (!empty($routes)) {
            $url .= '/'.implode('/', $routes);
        }
        return $url;
    }

    public function response($response)
    {
        return json_decode($response, 1);
    }

	protected function request($url, $headers = [])
    {
		$request = curl_init();
        curl_setopt($request, CURLOPT_URL, $url);
        curl_setopt($request, CURLOPT_HEADER, false);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($request, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($request, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($request, CURLOPT_HTTPHEADER, $headers);
        return $request;
	}

    public function post($routes = [], $params = [], $headers = [])
    {
        $headers    = $this->headers($headers);
        $params     = $this->params($params);
        $url        = $this->url($routes);
        $request    = $this->request($url, $headers);
        curl_setopt($request, CURLOPT_POST, true);
        curl_setopt($request, CURLOPT_POSTFIELDS, $params);
        $response   = $this->response($this->exec($request));
        return $response;
    }

    public function get($routes = [], $params = [], $headers = [])
    {
        $headers    = $this->headers($headers);
        $params     = $this->params($params);
        $url        = $this->url($routes);
        $request    = $this->request($url.'?'.$params, $headers);
        $response   = $this->response($this->exec($request));
        return $response;
    }

    protected function exec($request)
    {
        try {
            $response = curl_exec($request);
        } catch (Exception $e) {
            return;
        } catch (ApplicationException $e) {
            return;
        }
        return $response;
    }
}
