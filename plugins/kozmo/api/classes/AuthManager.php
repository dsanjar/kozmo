<?php namespace Kozmo\Api\Classes;

use Request;
use Validator;
use Firebase\JWT\JWT;
use \InvalidArgumentException;
use \UnexpectedValueException;
use \SignatureInvalidException;
use Mobizon\MobizonApi;
use Kozmo\Personal\Models\User;
use Kozmo\Personal\Models\Settings;
use Kozmo\Personal\Models\SmsCodes;
use Kozmo\Api\Classes\ApiHelper;
use Kozmo\Api\Classes\ResponseManager;
use Kozmo\Common\Models\QrCode as QrCodeModel;

class AuthManager
{
    use \October\Rain\Support\Traits\Singleton;

    protected $allowedSmsRepeatTime;

    private $mobizonApiKey;

    private $jwtExpirationTime;

    private $smsCodeText;


    protected function init()
    {
        $this->mobizonApiKey        = Settings::get('mobizonApiKey');
        $this->smsCodeText          = Settings::get('smsCodeText');
        $this->allowedSmsRepeatTime = ApiHelper::stringToSeconds(Settings::get('allowedSMSRepeatTime'));
        $this->jwtExpirationTime    = ApiHelper::stringToSeconds(Settings::get('jwtExpirationTime'));
    }

    /**
     * @return array|int
     * @internal param Request $request
     */
    public function signIn()
    {
        $post = post();
        $phone = trim(str_replace(' ', '', $post['phone']));
        if (!$this->phoneValidate($phone)) {
            return $this->error('phone-bad-format');
        }
        if (!$user = User::byPhone($phone)->enabled()->first()) {
            return $this->error('user-not-found');
        }
        if ($send = $this->sendSms($phone)) {
            return $send;
        }
        return $this->success(['result' => 'Access code has sent via sms.']);
    }

    /**
     * @return array
     * @internal param Request $request
     */
    public function signUp($returnUser = false, $post = null)
    {
        $post = (empty($post))? post(): $post;
        $data = [];
        $rules = [
            'name'      => 'required|string',
            'surname'   => 'required|string',
            'phone'     => 'required|size:12',
            'email'     => 'email'
        ];

        if (isset($post['name'])) {
            $data['name'] = trim(str_replace(' ', '', $post['name']));
        }
        if (isset($post['surname'])) {
            $data['surname'] = trim(str_replace(' ', '', $post['surname']));
        }
        if (isset($post['phone'])) {
            $data['phone'] = trim(str_replace(' ', '', $post['phone']));
            $data['phone'] = str_replace('(', '', $data['phone']);
            $data['phone'] = str_replace(')', '', $data['phone']);
            $data['phone'] = str_replace('-', '', $data['phone']);
        }
        if (isset($post['email'])) {
            $data['email'] = trim(str_replace(' ', '', $post['email']));
        }

        if (!$user = User::byPhone($data['phone'])->disabled()->first()) {
            $rules['phone'] = 'required|size:12|unique:kozmo_personal_users';
            $rules['email'] = 'email|unique:kozmo_personal_users';
        }

        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return $this->error('user-bad-format', $validator->messages()->toArray());
        }

        if (!$user = User::byPhone($data['phone'])->disabled()->first()) {
            $user = new User();
            $user->name     = $data['name'];
            $user->surname  = $data['surname'];
            $user->phone    = $data['phone'];
            if (isset($post['email'])) {
                $user->email = $data['email'];
            }
            $user->is_enabled = false;
            if (!$user->save()) {
                return $this->error('user-registration');
            }
        }
        if ($returnUser) {
            return ['user' => $user];
        }
        if ($send = $this->sendSms($data['phone'])) {
            return $send;
        }
        return $this->success(['id' => $user->id, 'result' => 'Access code has been sent via sms.']);
    }

    public function getTestUser()
    {
        return User::first();
    }

    /**
     * @param $phone
     * @return bool|string
     * @internal param Request $request
     */
    public function phoneValidate($phone)
    {
        $validator = Validator::make(
            ['phone' => $phone],
            ['phone' => 'required|size:12']
        );

        if ($validator->passes()) {
            return true;
        }
        return false;
    }

    public function isExpiredSMS($phone)
    {
        if (!$smsCode = SmsCodes::byPhone($phone)->isNotUsed()->latest()->first()) {
            return true;
        }
        $currentTimestamp = time();
        $allowedTime = strtotime($smsCode->created_at) + $this->allowedSmsRepeatTime;
        if ($currentTimestamp < $allowedTime) {
            return false;
        }
        $smsCode->setUsed();
        return true;
    }

    public function generateSMSCode($phone)
    {
        $random             = rand(100000, 999999);
        $smsCode            = new SmsCodes();
        $smsCode->phone     = $phone;
        if (env('APP_TEST_ACCOUNT') == $phone) {
            $smsCode->sms_code  = env('APP_TEST_SMSCODE');
        } else {
            $smsCode->sms_code  = $random;
        }
        if (!$smsCode->save()) {
            return false;
        }
        return $smsCode;
    }

    /**
     * @param $phone
     * @return mixed|void
     * @internal param $recipient
     * @internal param $text
     */
    public function sendSMS($phone)
    {
        if (!$this->isExpiredSMS($phone)) {
            return $this->error('sms-code-generate');
        }
        if (!$smsCode = $this->generateSMSCode($phone)) {
            return $this->error('sms-database');
        }
        if (!empty($this->mobizonApiKey)) {
            $phone  = str_replace('+', '', $phone);
            $text   = ApiHelper::parseSmsCodeText($this->smsCodeText, $smsCode->sms_code);
            $api    = new MobizonApi($this->mobizonApiKey);
            $params     = [
                //    'from'       => 'Kozmo',
                'recipient' => $phone,
                'text'      => $text,
            ];
            if (!env('API_USE_SMS')) {
                return $this->success($params);
            }
            if ($api->call('message', 'sendSMSMessage', $params) === false) {
                return $this->error('sms-send');
            }
        }
        return;
    }

    /**
     * @return array
     * @internal param Request $request
     */
    public function smsConfirm()
    {
        $post = post();
        if (!isset($post['phone'])) {
            return $this->error('phone-empty');
        }
        $phone = trim(str_replace(' ', '', $post['phone']));
        if (!$this->phoneValidate($phone)) {
            return $this->error('phone-bad-format');
        }
        if (!$user = User::byPhone($phone)->first()) {
            return $this->error('user-not-found');
        }
        $smsCode = $post['sms_code'];
        if (empty($smsCode)) {
            return $this->error('sms-code-empty');
        }
        if (!$smsCodeModel = SmsCodes::byPhoneCode($phone, $smsCode)->isNotUsed()->latest()->first()) {
            return $this->error('sms-code-not-found');
        }
        if (!$smsCodeModel->setUsed()) {
            return $this->error('sms-database');
        }
        if (!$user->setEnabled()) {
            return $this->error('sms-database');
        }

        return $this->success([
            'phone'     => $phone,
            'sms_code'  => $smsCode,
            'jwt'       => $this->encodeJWT($user)
        ]);
    }

    /**
     * @return mixed
     * @internal param Request $request
     */
    public function smsRepeat()
    {
        return $this->signIn();
    }

    /**
     * Check JSON Web Token
     *
     * @param null $jwt
     * @return array|string
     * @internal param Request $request
     */
    public function checkJWT($jwt = null)
    {
        if (!$jwt) {
            $jwt = request()->header('auth-token');
        }
        if (!$decoded = $this->decodeJWT($jwt)) {
            return false;
        }
        if (!(($decoded->iss === config('url')) && ($decoded->exp > time()))) {
            return false;
        }
        return true;
    }

    /**
     * Get User By JSON Web Token
     *
     * @param null $jwt
     * @return array|string
     * @internal param Request $request
     */
    public function getUserByJWT($jwt = null)
    {
        if (!$jwt) {
            $jwt = request()->header('auth-token');
        }
        if (!$decoded = $this->decodeJWT($jwt)) {
            return false;
        }
        if (!$user = User::byPhone($decoded->phone)->first()) {
            return false;
        }
        return $user;
    }

    public function encodeJWT($userInfo)
    {
        $token  = [
            'iss'     => config('url'),
            'aud'     => config('url'),
            'iat'     => time(),
            'exp'     => time() + $this->jwtExpirationTime, // Three months until expiration
            'name'    => $userInfo['name']    ? $userInfo['name']    : null,
            'surname' => $userInfo['surname'] ? $userInfo['surname'] : null,
            'phone'   => $userInfo['phone']   ? $userInfo['phone']   : null
        ];

        JWT::$leeway = 60;
        $key         = env('APP_KEY');
        $jwt         = JWT::encode($token, $key);
        return $jwt;
    }

    public function decodeJWT($jwt)
    {
        if (empty($jwt)) {
            return false;
        }
        try {
            JWT::$leeway = 60;
            $key         = env('APP_KEY');
            $decoded     = JWT::decode($jwt, $key, ['HS256']);
        } catch (SignatureInvalidException $e) {
            return false;
        } catch (InvalidArgumentException $e) {
            return false;
        } catch (UnexpectedValueException $e) {
            return false;
        }
        return $decoded;
    }

    /**
     * Bad response
     * @param $key
     * @param $data
     * @return mixed
     */
    public function error($key, $data = [])
    {
        return ResponseManager::error($key, $data);
    }

    /**
     * Successful response
     * @param $data
     * @return mixed
     */
    public function success($data)
    {
        return ResponseManager::success($data);
    }

    /**
     * Throws an Exception in easier way
     * @param $key
     * @throws \Exception
     * @internal param array $data
     * @internal param $error
     */
    public function exception($key)
    {
        throw new \Exception(ResponseManager::$errors[$key]['error'], ResponseManager::$errors[$key]['code']);
    }
}
