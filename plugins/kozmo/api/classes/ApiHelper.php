<?php namespace Kozmo\Api\Classes;

use Kozmo\Api\Models\Settings as ApiSettings;
use Kozmo\Personal\Models\Settings as PersonalSettings;
use Kozmo\Api\Classes\ResponseManager;

class ApiHelper
{

    public static function checkToken($token)
    {
        if ($apiToken = env('API_TOKEN')) {
            return $token == $apiToken;
        }

        if ($apiToken = ApiSettings::get('api.token')) {
            return $token == $apiToken;
        }

        return;
    }

    public static function stringToSeconds($value)
    {
        $a      = explode("*", $value);
        $result = 1;
        foreach ($a as $i) {
            $result *= $i;
        }
        return $result !== 1 ? $result : 0;
    }

    public static function parseSmsCodeText($text, $code)
    {
        return str_replace('$XXXXXX', $code, $text);
    }

    /**
     * Bad response
     * @param $key
     * @param $data
     * @return mixed
     */
    public static function error($key, $data = [])
    {
        return ResponseManager::error($key, $data);
    }

    /**
     * Successful response
     * @param $data
     * @return mixed
     */
    public static function success($data)
    {
        return ResponseManager::success($data);
    }

    /**
     * Throws an Exception in easier way
     * @param $key
     * @param array $data
     * @return
     * @internal param $error
     */
    public static function exception($key, $data = [])
    {
        return ResponseManager::error($key, $data);
    }
}
