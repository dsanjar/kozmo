<?php namespace Kozmo\Common\Console;

use DB;
use Carbon\Carbon;
use ApplicationException;
use Kozmo\Common\Models\Shelf;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class SeedShelves extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'seed:shelves';

    /**
     * @var string The console command description.
     */
    protected $description = 'Сидим полки.';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $blocks = [
            'A' => [15, 6, 5, 10],
            'B' => [15, 8, 5, 5],
            'C' => [15, 6, 5, 5],
            'D' => [15, 6, 5, 1],
            'E' => [15, 6, 5, 1]
        ];
        foreach ($blocks as $letter => $block) {
            for ($i=1; $i <= $block[0]; $i++) {
                for ($j=1; $j <= $block[1]; $j++) {
                    $shelf = new Shelf;
                    $shelf->block = $letter;
                    $shelf->row = $i;
                    $shelf->col = $j;
                    $shelf->priority = ($i <= $block[2]) ? $block[3] : 0;
                    $shelf->save();
                }
            }
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }

}
