<?php namespace Kozmo\Common\Models;

use Model;
use Kozmo\Helpers\ImageHelper;

/**
 * QrCode Model
 */
class QrCode extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'kozmo_common_qr_codes';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array The attributes that should be appends in arrays.
     */
    protected $appends = ['is_occupied'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'user' => ['Kozmo\Personal\Models\User', 'key' => 'user_id']
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
    *   Scopes
    */
    public function scopeByCode($query, $value)
    {
        return $query->where('unique_string', $value);
    }

    public function scopeVacant($query)
    {
        return $query->whereNull('user_id');
    }

    public function scopePrintable($query)
    {
        return $query->where('is_printable', 1);
    }

    public function scopeNotPrintable($query)
    {
        return $query->where('is_printable', 0);
    }
    /**
    *   Attributes
    */
    public function getImagePathAttribute($value)
    {
      if (!$value) {
        return;
      }
      return ImageHelper::qrcodeUrl($value);
    }

    public function getIsOccupiedAttribute($value)
    {
        return $this->user_id != null;
    }
}
