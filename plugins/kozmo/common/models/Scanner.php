<?php namespace Kozmo\Common\Models;

use Model;
use Carbon\Carbon;
use Kozmo\Sport\Models\Room;

/**
 * Scanner Model
 */
class Scanner extends Model
{
    const TYPE_INFO = 'info';
    const TYPE_IN_OUT = 'in-out';
    const TYPE_ONLY_IN = 'only-in';
    const TYPE_ONLY_OUT = 'only-out';

    /**
     * @var string The database table used by the model.
     */
    public $table = 'kozmo_common_scanners';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     *   Scopes
     */

    /**
     * Active scanners
     *
     * @param $query
     * @return mixed
     */
    public function scopeEnabled($query)
    {
        return $query->where('is_enabled', 1);
    }

    /**
     * Scanners that let's you only inside
     *
     * @param $query
     * @return mixed
     */
    public function scopeOneDirection($query)
    {
        return $query->where('type', 'in');
    }

    /**
     * Scanners that let's you inside and outside
     *
     * @param $query
     * @return mixed
     */
    public function scopeBiDirection($query)
    {
        return $query->where('type', 'in-out');
    }

    public function scopeInOut($query)
    {
        return $query->whereType(self::TYPE_IN_OUT);
    }

    public function scopeOnlyIn($query)
    {
        return $query->whereType(self::TYPE_ONLY_IN);
    }

    public function scopeOnlyOut($query)
    {
        return $query->whereType(self::TYPE_ONLY_OUT);
    }

    public function scopeInfo($query)
    {
        return $query->whereType(self::TYPE_INFO);
    }

    /*
    *  Options
    */
    public function getTypeOptions()
    {
        return [
            self::TYPE_INFO => 'Информационный',
            self::TYPE_IN_OUT => 'Вход и выход',
            self::TYPE_ONLY_IN => 'Только вход',
            self::TYPE_ONLY_OUT => 'Только выход'
        ];
    }

    /*
    *  Attributes
    */
    public function getCurrentRoomAttribute()
    {
        $room = Room::closest()->notClosed()->first();
        return $room;
    }

    public function getRandomMessageInAttribute()
    {
        $messages = explode("\r\n", $this->messages_in);
        $random = rand(0, count($messages)-1);
        return $messages[$random];
    }

    public function getRandomMessageOutAttribute()
    {
        $messages = explode("\r\n", $this->messages_out);
        $random = rand(0, count($messages)-1);
        return $messages[$random];
    }

    /*
    *  Methods
    */
    public function isOnlyIn()
    {
        return $this->type == self::TYPE_ONLY_IN;
    }

    public function isInOut()
    {
        return $this->type == self::TYPE_IN_OUT;
    }
}
