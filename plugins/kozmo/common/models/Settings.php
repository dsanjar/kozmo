<?php namespace Kozmo\Common\Models;

use System\Models\MailTemplate;
use October\Rain\Database\Model;

class Settings extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];
    public $settingsCode = 'kozmo_common_settings';
    public $settingsFields = 'fields.yaml';

    public $rules = [
        'room_email' => 'email',
        'room_copy'  => 'email',
    ];

    public function mailTemplates()
    {
        $codes = array_keys(MailTemplate::listAllTemplates());
        $result = [''=>'- Do not send notification -'];
        $result += array_combine($codes, $codes);
        return $result;
    }
}
