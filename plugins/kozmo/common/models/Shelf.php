<?php namespace Kozmo\Common\Models;

use Model;

/**
 * Shelf Model
 */
class Shelf extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'kozmo_common_shelves';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Appends fields
     */
    protected $appends = ['title'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'user' => ['Kozmo\Personal\Models\User', 'key' => 'user_id']
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
    *   Scopes
    */
    public function scopeEnabled($query)
    {
        return $query->where('is_enabled', 1);
    }

    public function scopeVacant($query)
    {
        return $query->where('is_occupied', 0);
    }

    public function scopeOccupied($query)
    {
        return $query->where('is_occupied', 1);
    }

    public function scopeByPriority($query)
    {
        return $query->orderBy('priority', 'desc')
            ->orderBy('block', 'asc')
            ->orderBy('row', 'asc')
            ->orderBy('col', 'asc');
    }
    /**
    *   Scopes
    */
    public function beforeSave()
    {
        $this->is_occupied = !empty($this->user_id);
    }

    /**
    *   Attributes
    */
    public function getTitleAttribute($value)
    {
        return $this->block.'.'.$this->row.'.'.$this->col;
    }
}
