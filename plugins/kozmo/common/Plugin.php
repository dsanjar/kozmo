<?php namespace Kozmo\Common;

use Event;
use Backend;
use System\Classes\PluginBase;

/**
 * Common Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Common',
            'description' => 'No description provided yet...',
            'author'      => 'Kozmo',
            'icon'        => 'icon-database'
        ];
    }

    /**
     * Register settings, called when the plugin is first registered.
     *
     * @return void
     */
    public function registerSettings()
    {
        return [
            'settings' => [
                'category'    => 'Common',
                'label'       => 'Settings',
                'description' => 'Manage common settings.',
                'icon'        => 'icon-plug',
                'class'       => 'Kozmo\Common\Models\Settings',
                'order'       => 500,
                'keywords'    => 'common email',
                'permissions' => ['kozmo.common.setting']
            ]
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        $this->registerConsoleCommand('seed.shelves', 'Kozmo\Common\Console\SeedShelves');
        $this->registerConsoleCommand('generatie.qrcodes', 'Kozmo\Common\Console\GenerateQrcodes');
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        Event::listen('check.in', 'Kozmo\Common\Handlers\CheckinEventHandler@handleIn');
        Event::listen('check.out', 'Kozmo\Common\Handlers\CheckinEventHandler@handleOut');
        Event::listen('room.in', 'Kozmo\Common\Handlers\RoomsEventHandler@handleIn');
        Event::listen('room.out', 'Kozmo\Common\Handlers\RoomsEventHandler@handleOut');
        Event::listen('crm.deal.add', 'Kozmo\Common\Handlers\CRMEventHandler@addDeal');
        Event::listen('crm.deal.updated', 'Kozmo\Common\Handlers\CRMEventHandler@updateDeal');
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Kozmo\Common\Components\ScannerComponent' => 'scanner',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'kozmo.common.common' => [
                'tab' => 'Common',
                'label' => 'Common permission'
            ],
            'kozmo.common.settings' => [
                'tab' => 'Common',
                'label' => 'Settings permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'common' => [
                'label'       => 'Common',
                'url'         => Backend::url('kozmo/common/scanner'),
                'icon'        => 'icon-database',
                'permissions' => ['kozmo.common.*'],
                'order'       => 300,
                'sideMenu' => [
                    'splash' => [
                        'label' => 'Splashes',
                        'url'   => Backend::url('kozmo/common/splash'),
                        'icon'  => 'icon-circle-thin',
                        'permissions' => ['kozmo.common.*'],
                        'order' => 10
                    ],
                    'contact' => [
                        'label' => 'Contacts',
                        'url'   => Backend::url('kozmo/common/contact'),
                        'icon'  => 'icon-circle-thin',
                        'permissions' => ['kozmo.common.*'],
                        'order' => 20
                    ],
                    'scanner' => [
                        'label' => 'Scanner',
                        'url'   => Backend::url('kozmo/common/scanner'),
                        'icon'  => 'icon-circle-thin',
                        'permissions' => ['kozmo.common.*'],
                        'order' => 30
                    ],
                    'shelves' => [
                        'label' => 'Shelves',
                        'url'   => Backend::url('kozmo/common/shelf'),
                        'icon'  => 'icon-circle-thin',
                        'permissions' => ['kozmo.common.*'],
                        'order' => 40
                    ],
                ]
            ],
        ];
    }
}
