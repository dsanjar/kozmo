<?php namespace Kozmo\Common\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use System\Classes\SettingsManager;

/**
 * Location Back-end Controller
 */
class Location extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $relationConfig = 'config_relation.yaml';

    public function __construct()
    {
        parent::__construct();

        // BackendMenu::setContext('Kozmo.Common', 'common', 'location');
        BackendMenu::setContext('October.System', 'system', 'settings');
        SettingsManager::setContext('Kozmo.Common', 'location');
    }
}
