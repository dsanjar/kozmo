<?php namespace Kozmo\Common\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Splash Back-end Controller
 */
class Splash extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.ReorderController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Kozmo.Common', 'common', 'splash');
    }

    public function onReorder()
    {
        parent::onReorder();
        return $this->listRefresh();
    }
}
