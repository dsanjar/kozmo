<?php namespace Kozmo\Common\Handlers;

use Log;
use Mail;
use Kozmo\Common\Models\Settings;

class RoomsEventHandler {

	public function handle($data)
	{
		Log::info('Handle room event');
	}

	public function handleIn($room, $user)
	{
		Log::info('Handle in room');
		if (!$template = Settings::get('room_in_template')) {
			return;
		}
		if (!$to = Settings::get('room_email')) {
			return;
		}
        $copy = Settings::get('room_copy') ? Settings::get('room_copy') : null;
        try{
            $params = [
                'room' => $room,
                'user' => $user,
            ];
            Mail::send($template, $params, function($message) use($to, $copy) {
                $message->to($to, 'Operator');
                if ($copy) {
                    $message->bcc($copy, 'Manager');
                }
            });
        }catch(\Swift_TransportException $e){
            Log::debug('Rooms report mail send swift exception. '. $e->getMessage());
        }catch(\Exception $e){
            Log::debug('Rooms report mail send exception. '.$e->getMessage());
        }
	}

	public function handleOut($room, $user)
	{
		Log::info('Handle out room');
		if (!$template = Settings::get('room_out_template')) {
			return;
		}
		if (!$to = Settings::get('room_email')) {
			return;
		}
        $copy = Settings::get('room_copy') ? Settings::get('room_copy') : null;
        try{
            $params = [
                'room' => $room,
                'user' => $user,
            ];
            Mail::send($template, $params, function($message) use($to, $copy) {
                $message->to($to, 'Operator');
                if ($copy) {
                    $message->bcc($copy, 'Manager');
                }
            });
		}catch(\Swift_TransportException $e){
            Log::debug('Rooms report mail send swift exception. '. $e->getMessage());
        }catch(\Exception $e){
            Log::debug('Rooms report mail send exception. '.$e->getMessage());
        }
	}
}
