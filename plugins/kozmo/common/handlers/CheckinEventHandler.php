<?php namespace Kozmo\Common\Handlers;

use Log;

class CheckinEventHandler {

	public function handle($data)
	{
		Log::info('Handle event '.$data->id);
	}

	public function handleIn($checkin, $isReduce = false)
	{
		Log::info('Handle in '.$checkin->id);
		if ($isReduce) {
			if (!$user = $checkin->user) {
				Log::error('Handle in user required.');
			}
			if (!$scanner = $checkin->scanner) {
				Log::error('Handle in scanner required.');
			}
			if ($scanner->isOnlyIn()) {
				if ($subscription = $user->subscriptions()->byProductYoga()->hasCount()->enabled()->first()) {
					$checkin->subscription_id = $subscription->id;
					$checkin->save();
					if (!$subscription->is_frozen) {
						$subscription->visits_limit -= 1;
						$subscription->save();
					}
				}
			} elseif ($scanner->isInOut()) {
				if ($subscription = $user->subscriptions()->byProductSport()->hasCount()->enabled()->first()) {
					$checkin->subscription_id = $subscription->id;
					$checkin->save();
					if (!$subscription->is_frozen) {
						$subscription->visits_limit -= 1;
						$subscription->save();
					}
				}
			}
			// if ($subscription = $user->subscriptions()->free()->hasCount()->enabled()->first()) {
	        //     $subscription->visits_limit -= 1;
	        //     $subscription->save();
	        // } else {
			// 	if ($scanner->isOnlyIn()) {
			// 		if ($subscription = $user->subscriptions()->hasCount()->enabled()->first()) {
			// 			if (!$subscription->is_frozen) {
			// 				$subscription->visits_limit -= 1;
			// 				$subscription->save();
			// 			}
			// 		}
			// 	}
			// }
			// $user->reduceVisitsLimit();
			Log::info('Handle with reduce limits.');
		}
	}

	public function handleOut($checkin, $isRevert = false)
	{
		Log::info('Handle out '.$checkin->id);
		if ($isRevert) {
			if (!$user = $checkin->user) {
				Log::error('Handle in user required.');
			}
			if (!$scanner = $checkin->scanner) {
				Log::error('Handle in scanner required.');
			}
			if ($scanner->isOnlyIn()) {
				if ($subscription = $user->subscriptions()->byProductYoga()->hasCount()->enabled()->first()) {
					$subscription->visits_limit += 1;
					$subscription->save();
					$checkin->delete();
				}
			}
		}
	}
}
