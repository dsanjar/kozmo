<?php namespace Kozmo\Common\Handlers;

use Log;
use Kozmo\Api\Classes\CurlManager;

class CRMEventHandler {


	public function addDeal($user = null, $order = null, $subs = null)
	{
		if (!$user) {
			return;
		}
		if (!$order) {
			return;
		}
		if (!$subs) {
			return;
		}
		if (!$product = $subs->product) {
			return;
		}
		if ($product->is_free) {
			return;
		}
		Log::info('Add deal for subs '.$subs->id);
		$curl = CurlManager::instance();

		// Add deal
		$fields = [
			"TITLE" => $product->title,
			"TYPE_ID" => 'COMPLEX',
			"STAGE_ID" => 'WON',
			"OPENED" => 'N',
			"CLOSED" => 'Y',
			"CURRENCY_ID" => 'KZT',
			"UF_CRM_1509363822" => '82',
			"UF_CRM_1509512493" => ($product->type == 'sport')? '88': '86',
			"OPPORTUNITY" => $product->price,
			"BEGINDATE" => $subs->created_at->toAtomString(),
			"CLOSEDATE" => $subs->updated_at->toAtomString(),
			"UF_CRM_1509512644853" => $subs->updated_at->toAtomString()
		];
		$params = [
			"REGISTER_SONET_EVENT" => "Y"
		];
		$response = $curl->post(
			['crm.deal.add'],[
				'fields' => $fields,
				'params' => $params
			]
		);
		if (!$response) {
			Log::info('Response empty for subs '.$subs->id);
			return;
		}
		if (!isset($response['result'])) {
			Log::info('Response result empty for subs '.$subs->id);
			return;
		}
		$deal = $response['result'];
		Log::info('Created deal '.$deal.' success.');

		$order->crm_id = $deal;
		$order->save();

		// Add product
		$rows = [[
			"PRODUCT_ID" => $product->crm_id,
			"PRICE" 	 => $product->price,
			"QUANTITY"   => 1
		]];
		$response = $curl->post(
			['crm.deal.productrows.set'],[
				'id' => $deal,
				'rows' => $rows
			]
		);
		Log::info('Added product success.');

		// Find contact
		$response = $curl->post(
			['crm.duplicate.findbycomm'],
			[
				'entity_type' => 'CONTACT',
				'type' => 'PHONE',
				'values' => [$user->phone, $user->getPhoneWithPrefix()]
			]
		);

		if ($response && isset($response['result']) && !empty($response['result'])) {
			// Get found contact
			$contact = $response['result']['CONTACT'][0];
			Log::info('Found contact '.$contact);
		} else {
			// Create contact
			$fields = [
				"NAME" => $user->name,
				"LAST_NAME" => $user->surname,
				"OPENED" => 'Y',
				"TYPE_ID" => 'CLIENT',
				"SOURCE_ID" => 'WEB',
				"UF_CRM_1508740212" => ($user->shelf)? $user->shelf->title: '',
				"PHONE" => [["VALUE" => $user->phone, "VALUE_TYPE" => "WORK"]]
			];
			$params = [
				"REGISTER_SONET_EVENT" => "Y"
			];
			$response = $curl->post(
				['crm.contact.add'],[
					'fields' => $fields,
					'params' => $params
				]
			);
			if (!$response) {
				Log::info('Response empty for user '.$user->id);
				return;
			}
			if (!isset($response['result'])) {
				Log::info('Response result empty for user '.$user->id);
				return;
			}
			$contact = $response['result'];
			Log::info('Created contact '.$contact);
		}

		// Add contact
		$response = $curl->post(
			['crm.deal.contact.items.set'],[
				'id' => $deal,
				'items' => [[
					"CONTACT_ID" => $contact
				]]
			]
		);

		Log::info('Added contact success.');
		return;
	}

}
