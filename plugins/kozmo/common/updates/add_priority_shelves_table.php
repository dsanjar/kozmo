<?php namespace Kozmo\Common\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddPriorityShelvesTable extends Migration
{
    public function up()
    {
        Schema::table('kozmo_common_shelves', function(Blueprint $table) {
           $table->integer('priority')->default(0);
        });
    }

    public function down()
    {
        Schema::table('kozmo_common_shelves', function(Blueprint $table) {
            $table->dropColumn('priority');
        });
    }
}
