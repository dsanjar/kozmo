<?php namespace Kozmo\Common\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddUserIdQrCodesTable extends Migration
{
    public function up()
    {
        Schema::table('kozmo_common_qr_codes', function(Blueprint $table) {
           $table->integer('user_id')->after('image_path')->nullable();
        });
    }

    public function down()
    {
        Schema::table('kozmo_common_qr_codes', function(Blueprint $table) {
            $table->dropColumn('user_id');
        });
    }
}
