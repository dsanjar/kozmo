<?php namespace Kozmo\Common\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddTypeScannersTable extends Migration
{
    public function up()
    {
        Schema::table('kozmo_common_scanners', function(Blueprint $table) {
           $table->string('type')->after('name')->default('in-out');
        });
    }

    public function down()
    {
        Schema::table('kozmo_common_scanners', function(Blueprint $table) {
            $table->dropColumn('type');
        });
    }
}
