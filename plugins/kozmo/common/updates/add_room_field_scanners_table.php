<?php namespace Kozmo\Common\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddRoomFieldToScannersTable extends Migration
{
    public function up()
    {
        Schema::table('kozmo_common_scanners', function(Blueprint $table) {
           $table->integer('group_id')->nullable();
        });
    }

    public function down()
    {
        Schema::table('kozmo_common_scanners', function(Blueprint $table) {
            $table->dropColumn('group_id');
        });
    }
}
