<?php namespace Kozmo\Common\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddContentFieldsToScannersTable extends Migration
{
    public function up()
    {
        Schema::table('kozmo_common_scanners', function(Blueprint $table) {
           $table->string('title')->nullable();
           $table->text('summary')->nullable();
           $table->text('messages_in')->nullable();
           $table->text('messages_out')->nullable();
        });
    }

    public function down()
    {
        Schema::table('kozmo_common_scanners', function(Blueprint $table) {
            $table->dropColumn('title');
            $table->dropColumn('summary');
            $table->dropColumn('messages_in');
            $table->dropColumn('messages_out');
        });
    }
}
