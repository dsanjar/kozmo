<?php namespace Kosmo\Feedback;

use Backend;
use System\Classes\PluginBase;

/**
 * Feedback Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Feedback',
            'description' => 'No description provided yet...',
            'author'      => 'Kosmo',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {

        return [
            'Kosmo\Feedback\Components\FeedbackForm' => 'feedback',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'kosmo.feedback.common' => [
                'tab' => 'Feedback',
                'label' => 'Common permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {

        return [
            'feedback' => [
                'label'       => 'Feedback',
                'url'         => Backend::url('kosmo/feedback/profile'),
                'icon'        => 'icon-leaf',
                'permissions' => ['kosmo.feedback.common'],
                'order'       => 500,
            ],
        ];
    }
}
