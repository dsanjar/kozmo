<?php namespace Kosmo\Feedback\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Profile Back-end Controller
 */
class Profile extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Kosmo.Feedback', 'feedback', 'profile');
    }
}
