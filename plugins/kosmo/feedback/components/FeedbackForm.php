<?php namespace Kosmo\Feedback\Components;

use Redirect;
use Validator;
use Carbon\Carbon;
use ValidationException;
use ApplicationException;
use Kosmo\Feedback\Models\Profile;
use Cms\Classes\ComponentBase;

class FeedbackForm extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'FeedbackForm Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {

    }

    public function onSubmit()
    {
        if (!$phone = post('phone')) {
            return Redirect::refresh();
        }
        $phone = str_replace(' ', '', $phone);
        $phone = str_replace('(', '', $phone);
        $phone = str_replace(')', '', $phone);
        $phone = str_replace('-', '', $phone);

        $rules = [
            'phone' => 'required|size:12|unique:kosmo_feedback_profiles'
        ];

        $validation = Validator::make(['phone' => $phone], $rules);
        if ($validation->fails()) {
            throw new ValidationException($validation);
        }

        $profile = new Profile;
        $profile->phone = $phone;
        $profile->is_selected = false;
        $profile->save();

        return Redirect::to('/feedback/success');
    }

    public function onRandom()
    {
        $profiles = Profile::all();
        if ($profiles->isEmpty()) {
            return;
        }
        $random = $profiles->random(1);
        if (!$profile = $random[0]) {
            return;
        }
        $profile->is_selected = true;
        $profile->save();

        $this->page['phone'] = $profile->phone;
        return;
    }
}
