<?php namespace Kosmo\Export;

use Backend;
use System\Classes\PluginBase;

/**
 * export Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Export',
            'description' => 'Export plugin',
            'author'      => 'Kosmoport',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Kosmo\Export\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'kosmo.export.common_perimission' => [
                'tab' => 'Report',
                'label' => 'Common permission'
            ],
            'kosmo.export.export_report' => [
                'tab' => 'Report',
                'label' => 'Export report'
            ]
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'export' => [
                'label'       => 'Отчеты',
                'url'         => Backend::url('kosmo/export/report'),
                'icon'        => 'icon-folder-o',
                'permissions' => ['kosmo.export.*'],
                'order'       => 200,
                'sideMenu'    => [
                    'user' => [
                        'label'     => 'Отчеты',
                        'icon'        => 'icon-folder-o',
                        'url'       => Backend::url('kosmo/export/report'),
                        'permissions' => ['kosmo.export.*'],
                    ],
                ]
            ],
        ];
    }

    public function register()
    {
        $this->registerConsoleCommand('report.check', 'Kosmo\Export\Console\CheckReport');
        $this->registerConsoleCommand('report.trainers', 'Kosmo\Export\Console\TrainersReport');
        $this->registerConsoleCommand('report.rooms', 'Kosmo\Export\Console\RoomsReport');
    }

    public function registerSchedule($schedule)
    {
        $schedule->command('report:check')->everyTenMinutes()->withoutOverlapping();
    }

}
