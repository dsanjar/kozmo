<?php namespace Kosmo\Export\Models;

use Model;
use Artisan;

/**
 * Report Model
 */
class Report extends Model
{
    const STATUS_WAITING = 'wating';
    const STATUS_PROGRESS = 'progress';
    const STATUS_SUCCESS = 'success';
    const STATUS_ERROR = 'error';

    const TYPE_TRAINERS = 'report:trainers';
    const TYPE_ROOMS = 'report:rooms';

    /**
     * @var string The database table used by the model.
     */
    public $table = 'kosmo_export_reports';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Datable fields
     */
    protected $dates = ['begin_at', 'end_at'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function getStatusOptions()
    {
        return [
            self::STATUS_WAITING => 'Wating',
            self::STATUS_PROGRESS => 'Progress',
            self::STATUS_SUCCESS => 'Success',
            self::STATUS_ERROR => 'Error'
        ];
    }

    public function getTypeOptions()
    {
        return [
            self::TYPE_TRAINERS => 'Отчет по тренерам',
            self::TYPE_ROOMS => 'Отчет по занятиям',
        ];
    }

    public function beforeCreate()
    {
        $types = $this->getTypeOptions();
        $this->title = $types[$this->type];
        $this->begin_at = $this->begin_at->startOfDay();
        $this->end_at = $this->end_at->endOfDay();
        $this->status = self::STATUS_WAITING;
    }

    public function generate()
    {
        Artisan::call($this->type, ['report' => $this->id]);
    }

    public function setSuccess()
    {
        $this->status = self::STATUS_SUCCESS;
        $this->save();
    }

    public function setError()
    {
        $this->status = self::STATUS_ERROR;
        $this->save();
    }

    public function scopeWaiting($query)
    {
        return $this->where('status', self::STATUS_WAITING);
    }

    public function scopeProgress($query)
    {
        return $this->where('status', self::STATUS_PROGRESS);
    }
}
