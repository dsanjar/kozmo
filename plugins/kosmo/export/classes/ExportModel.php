<?php namespace Kosmo\Export\Classes;

use Backend\Models\ExportModel as Model;
use Response;
use File;
use PHPExcel;
use PHPExcel_Cell;
use PHPExcel_IOFactory;
use ApplicationException;

class ExportModel extends Model
{
    protected $chunk = 1000;

    /**
     * Set query.
     * @return Query object
     */
    protected function queryData() {}

    /**
     * Mutate data before export.
     * @return Function closure
     */
    protected function mutateDate() {
        return function($key, $value) {
            return $value;
        };
    }

	public function exportData($columns, $sessionKey = null) {
        $chunk = $this->chunk;
        $query = $this->queryData();
        $mutator = $this->mutateDate();
        $result = [];
        $query->chunk($this->chunk, function($data) use ($columns, $mutator, &$result){
            foreach ($data as $row) {
                $values = [];
                foreach ($row as $key => $value) {
                    if (in_array($key, $columns)) {
                        $values[$key] = $mutator($key, $value);
                    }
                }
                $result[] = $values;
            }
        });
        return $result;
    }

	/**
	 * Download a previously compiled export file.
	 * @return void
	 */
	public function download($name, $outputName = null)
	{
		if (!preg_match('/^oc[0-9a-z]*$/i', $name)) {
			throw new ApplicationException('File not found');
		}

		$csvPath = temp_path() . '/' . $name;
		if (!file_exists($csvPath)) {
			throw new ApplicationException('File not found');
		}

		$headers =  [
			'Content-Type' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
			'Content-Disposition' => 'attachment;filename="export'.time().'.xlsx"',
			'Cache-Control' => 'max-age=0'
		];
		$result = Response::make(File::get($csvPath), 200, $headers);

		@unlink($csvPath);

		return $result;
	}


	public static function xyToL($y,$x)
	{
		return PHPExcel_Cell::stringFromColumnIndex($x) . ($y+1);
	}

	/**
     * Converts a data collection to a XLS file.
     */
    protected function processExportData($columns, $results, $options)
    {
        /*
         * Validate
         */
        if (!$results) {
            throw new ApplicationException('There was no data supplied to export');
        }

        /*
         * Parse options
         */
        $defaultOptions = [
            'useOutput' => false,
            'fileName' => 'export.xlsx',
            'delimiter' => null,
            'enclosure' => null,
            'escape' => null
        ];

        $options = array_merge($defaultOptions, $options);
        $columns = $this->exportExtendColumns($columns);


        $objPHPExcel = new \PHPExcel();

    	$objPHPExcel->getProperties()
			->setCreator($_SERVER['HTTP_HOST'])
			->setLastModifiedBy($_SERVER['HTTP_HOST'])
			->setTitle("Office 2007 XLSX Test Document")
			->setSubject("Office 2007 XLSX Test Document")
			->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
			->setKeywords("office 2007 openxml php")
			->setCategory("Test result file");

        // Creating a worksheet
     	$objPHPExcel->setActiveSheetIndex(0);
     	$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
     	$objPHPExcel->getDefaultStyle()->getFont()->setSize(8);


        /*
         * Add headers
         */
        $headers = $this->getColumnHeaders($columns);
        foreach($headers as $col => $val)
            $objPHPExcel->getActiveSheet()->setCellValue(self::xyToL(0, $col), $val);

        /*
         * Add records
         */
        foreach ($results as $row => $result) {

            $data = $this->matchDataToColumns($result, $columns);
            foreach($data as $col => $val)
                $objPHPExcel->getActiveSheet()->setCellValue(self::xyToL($row+1, $col), $val);

        }


        /*
         * Output
         */

        $objPHPExcel->setActiveSheetIndex(0);

         /*
          * Save for download
          */
        $targetName = uniqid('oc');
        $targetPath = temp_path().'/'.$targetName;

    	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    	$objWriter->save($targetPath);
    	return $targetName;
    }
}
?>
