<?php namespace Kosmo\Export\Console;
use Log;
use DB;
use ApplicationException;
use Kozmo\Sport\Models\Room;
use Kozmo\Sport\Models\Trainer;
use Kozmo\Personal\Models\Checkin;
use Kosmo\Export\Models\Report;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class TrainersReport extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'report:trainers';

    /**
     * @var string The console command description.
     */
    protected $description = 'Отчет по тренерам.';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $report_id = $this->argument('report');
        if (!$report = Report::find($report_id)) {
            $report->message = 'Report is not found.';
            $report->setError();
        }

        $begin_at = $report->begin_at->copy()->startOfDay();
        $end_at = $report->end_at->copy()->endOfDay();

        $nameFile = uniqid('report_trainers_') . '.csv';
        $tmpFile = temp_path() . '/' . $nameFile;
        file_put_contents($tmpFile, '');

        if (!$trainers = Trainer::published()->byType('yoga')->get()) {
            return;
        }

        $csv = [];
        $csv[] = [
            'Преподаватель',
            'Клиент',
            'Телефон',
            'Дата и время посещения',
            'Занятие',
            'Стоимость',
            'Оплата преподавателя',
            'Оплата Kozmo',
            'Валюта'
        ];
        $currency = 'тенге';

        foreach ($trainers as $trainer) {
            if (!$rooms = $trainer->rooms()->where('started_at', '>=', $begin_at)->where('started_at', '<=', $end_at)->get()) {
                continue;
            }
            foreach ($rooms as $room) {
                if (!$checkins = $room->checkins) {
                    continue;
                }
                foreach ($checkins as $checkin) {
                    $roomPrice = $checkin->getRoomPrice();
                    //Log::info('REPORT: Room price '.$roomPrice.' at CHECKIN '.$checkin->id);
                    $csv[] = [
                        $trainer->fullname,
                        $checkin->user->surname.' '.$checkin->user->name,
                        $checkin->user->phone,
                        $checkin->checked_in_at->format('d.m.Y H:i'),
                        $room->title,
                        $roomPrice,
                        $roomPrice*0.4,
                        $roomPrice*0.6,
                        $currency
                    ];
                }
            }
        }
        // $csv[] = ['Trainer', 'Date', 'Room', 'Count'];
        // foreach ($trainers as $trainer) {
        //     if (!$rooms = $trainer->rooms()->where('started_at', '>=', $begin_at)->where('started_at', '<=', $end_at)->get()) {
        //         continue;
        //     }
        //     foreach ($rooms as $room) {
        //         $csv[] = [
        //             $trainer->fullname,
        //             $room->started_at->format('d.m.Y'),
        //             $room->title,
        //             $room->checkins()->count()
        //         ];
        //     }
        // }
        $file = fopen($tmpFile, 'a');
        foreach ($csv as $line) {
            fputcsv($file, $line);
        }
        fclose($file);

        $newFile = storage_path('app/media/'.$nameFile);
        copy($tmpFile, $newFile);
        @unlink($tmpFile);
        // file_put_contents($tmpFile, '');
        $report->file = '/storage/app/media/'.$nameFile;
        $report->setSuccess();
        return;
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['report', InputArgument::REQUIRED, 'Report ID is required.']
        ];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }

}
