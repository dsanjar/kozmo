<?php namespace Kosmo\Export\Console;

use DB;
use ApplicationException;
use Kozmo\Sport\Models\Room;
use Kosmo\Export\Models\Report;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class RoomsReport extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'report:rooms';

    /**
     * @var string The console command description.
     */
    protected $description = 'Отчет по занятиям.';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $report_id = $this->argument('report');
        if (!$report = Report::find($report_id)) {
            $report->message = 'Report is not found.';
            $report->setError();
        }
        $begin_at = $report->begin_at->copy()->startOfDay();
        $end_at = $report->end_at->copy()->endOfDay();

        $nameFile = uniqid('report_rooms_') . '.csv';
        $tmpFile = temp_path() . '/' . $nameFile;
        file_put_contents($tmpFile, '');
        //
        $csv = [];
        Room::published()->byDatePeriod($begin_at, $end_at)->chunk(100, function($rooms) use(&$csv) {
            $csv[] = ['ID', 'Room', 'Date', 'Trainer', 'Count'];
            foreach ($rooms as $room) {
                if ($count = $room->users()->count()) {
                    $csv[] = [
                        $room->id,
                        $room->title,
                        $room->started_at->format('d.m.Y H:i'),
                        $room->trainers()->first()->fullname,
                        $count
                    ];
                    $csv[] = [' ', 'User', 'Phone'];
                    foreach ($room->users as $user) {
                        $csv[] = [
                            ' ',
                            $user->name.' '.$user->surname,
                            $user->phone
                        ];
                    }
                    $csv[] = [' ', ' ', ' '];
                }
            }
        });
        if (empty($csv)) {
            $report->message = 'Empty data.';
            $report->setError();
        }
        //
        $file = fopen($tmpFile, 'a');
        foreach ($csv as $line) {
            fputcsv($file, $line);
        }
        fclose($file);

        $newFile = storage_path('app/media/'.$nameFile);
        copy($tmpFile, $newFile);
        @unlink($tmpFile);
        // file_put_contents($tmpFile, '');
        $report->file = '/storage/app/media/'.$nameFile;
        $report->setSuccess();
        return;
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['report', InputArgument::REQUIRED, 'Report ID is required.']
        ];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }

}
