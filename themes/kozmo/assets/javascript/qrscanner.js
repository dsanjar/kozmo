var state = {
	status: 'default' // default|error|success|loading
};

$(function(){
	console.log('Ready!');

	var scanner = new Instascan.Scanner({
		video: document.getElementById('scanner-preview'),
		backgroundScan: false
	});

	scanner.addListener('scan', function (content) {
		showCameraLoader();
		var data = [];
			data['code'] = content;
		if ($('#room').length > 0) {
			data['room_id'] = $('#room').data('room-id');
		}
	    $.request('onScan', {
			data: data,
			beforeUpdate: function(data) {
				if (data.status != 'nothing') {
					if ($('[data-id=checkin]').length >= 6) {
						$('#info').empty();
					}
				}
			},
			success: function(data) {
				hideCameraLoader();
				// scanner.start();
				this.success(data);
			}
		});
	});

	Instascan.Camera.getCameras().then(function (cameras) {
		console.log(cameras);
	    if (cameras.length > 0) {
	        scanner.start(cameras[0]);
	    } else {
	        console.error('No cameras found.');
	    }
	}).catch(function (e) {
	    console.error(e);
		if ($('#error').length > 0) {
			$('#error').addClass('active');
			$('#error').find('.error-message').text('Камера не найдена. Подключите камеру, и обновите страницу.');
		}
	});

	function showLoader() {
		$('#loader').addClass('active');
	}

	function hideLoader() {
		$('#loader').removeClass('active');
	}

	function showCameraLoader() {
		$('#camera-loader').addClass('active');
	}

	function hideCameraLoader() {
		$('#camera-loader').removeClass('active');
	}

	$(document).on('click', '#close', function(e) {
		e.preventDefault();
		if ($('#room').length > 0) {
			showLoader();
			var data = [];
				data['room_id'] = $('#room').data('room-id');
			$.request('onClose', {
				data: data,
				success: function(data) {
					hideLoader();
					this.success(data);
				}
			});
		}
	});

	if (scannerType == 'info') {
		$.request('onNotify');
		var timer = setInterval(function() {
			$.request('onNotify', {
				beforeUpdate: function(data) {
					if (data.status != 'nothing') {
						if ($('[data-id=checkin]').length >= 6) {
							$('#info').empty();
						}
					}
				},
				success: function(data) {
					this.success(data);
				}
			});
		}, 3000);
	}
})
