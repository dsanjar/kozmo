var scanner         = new Instascan.Scanner({video: document.getElementById('scanner-preview')});
var scanner_id      = document.getElementById('scanner_id') ? document.getElementById('scanner_id').value : undefined;
var scanner_type    = document.getElementById('scanner_type') ? document.getElementById('scanner_type').value : undefined;

// In case if URL in backend is different from actual visited URL
// replace API_URL to actual visited URL
var URL             = window.location.href;
var API_URL         = document.getElementById('url') ? document.getElementById('url').value : undefined;
var replaceIndex    = URL.indexOf('scanner');
var baseURL         = URL.substr(0, replaceIndex);
var baseAPI_URL     = API_URL ? API_URL.substr(0, replaceIndex) : undefined;
API_URL             = API_URL
                        ? (baseAPI_URL === baseURL ? API_URL : API_URL.replace(baseAPI_URL, baseURL))
                        : undefined;

var formData        = new FormData();
var direction       = 'checkin';
var redirectTimeout = document.getElementById('redirect_timeout')
                        ? parseInt(document.getElementById('redirect_timeout').value)
                        : undefined;
var scannerInfo     = {};
var visible         = {
    masterclass: {
        default: scanner_type === 'in',
        success: false,
        error: false
    },
    sport: {
        default: scanner_type === 'in-out',
        success: false,
        error: false
    }
};

var currentType = scanner_type
                        ? (visible.masterclass.default === true ? 'masterclass' : 'sport')
                        : undefined;

var yellowCircle        = document.getElementById('yellow-circle-container');
var currentDefault      = document.getElementById(currentType + '-default');
var currentSuccess      = document.getElementById(currentType + '-success');
var currentError        = document.getElementById(currentType + '-error');
var loaderModal         = document.getElementById('loader-modal');
var snapshotDefault     = document.getElementById('snapshot-' + currentType + '-default');
var snapshotSuccess     = document.getElementById('snapshot-' + currentType + '-success');
var snapshotError       = document.getElementById('snapshot-' + currentType + '-error');
var successContainer    = document.getElementById('success-container');
var errorContainer      = document.getElementById('error-container');
var snapshotUsername    = document.getElementById('snapshot-' + currentType + '-username');

// defineIfNeedsRedirect();

function defineIfNeedsRedirect() {
    var needsRedirect   = window.location.href.replace(API_URL, '');
    var chunks          = needsRedirect.split('-');
    scannerInfo         = {
        id: parseInt(chunks[2].split('/')[1]),
        type: chunks[1].toString().toLowerCase(),
        stage: parseInt(chunks[2].split('/')[0])
    };

    if (scannerInfo.type === 'master' || scannerInfo.type === 'sport') {
        if (scannerInfo.stage === 2 || scannerInfo.stage === 3) {
            setTimeout(function() {
                window.location.href = API_URL + 'scanner-' + scannerInfo.type + '-1/' + scannerInfo.id;
            }, redirectTimeout && redirectTimeout > 0 ? redirectTimeout : 2000);

        }
    }
}

function chooseDirection(param) {
    direction                   = param;
    var inactive                = direction === 'checkin' ? 'checkout' : 'checkin';

    var inactiveButton          = document.getElementById(inactive + '-button');
    inactiveButton.className   += ' inactive-button';

    var activeButton            = document.getElementById(direction + '-button');
    activeButton.className      = activeButton.className.replace('inactive-button', '');

    var borderColor = direction === 'checkin' ? checkinColor : checkoutColor;
    document.getElementById('scanner-preview').style.border = '5px solid ' + borderColor;

    document.getElementById('scanner-container').style.display = 'block';
}

function handleCheckin(params) {
    formData.append('scanner_id', params.scanner_id);
    formData.append('status', params.status);
    formData.append('unique_string', params.unique_string);

    return fetch(API_URL + 'api/v1/' + 'qrcode/' + direction, {
        method: 'POST',
        headers: {
            'api-token': '12345',
        },
        body: formData
    }).then(function (response) {
        return response.json();
    });
}

function success(user) {
    // Display loader
    loaderModal.style.visibility = 'visible';

    setTimeout(function() {
        // Hide
        currentDefault.style.display    = 'none';
        currentError.style.display      = 'none';
        snapshotDefault.style.display   = 'none';
        snapshotError.style.display     = 'none';
        if (yellowCircle) {
            yellowCircle.style.display = 'none';
        }
        errorContainer.style.display    = 'none';

        // Show
        currentSuccess.style.display    = 'block';
        snapshotSuccess.style.display   = 'block';
        successContainer.style.display  = 'block';

        // Add text
        snapshotUsername.innerHTML      = user.name + ' ' + user.surname;

        // Hide loader
        loaderModal.style.visibility    = 'hidden';
    }, 1500);
}

function error() {
    // Display loader
    loaderModal.style.visibility = 'visible';

    setTimeout(function () {
        // Hide
        currentDefault.style.display    = 'none';
        currentSuccess.style.display    = 'none';
        snapshotDefault.style.display   = 'none';
        snapshotSuccess.style.display   = 'none';
        if (yellowCircle) {
            yellowCircle.style.display = 'none';
        }
        successContainer.style.display  = 'none';

        // Show
        currentError.style.display      = 'block';
        snapshotError.style.display     = 'block';
        errorContainer.style.display    = 'block';

        // Hide loader
        loaderModal.style.visibility    = 'hidden';
    }, 1500);
}

scanner.addListener('scan', function (content) {
    handleCheckin({
        scanner_id: scanner_id,
        status: direction,
        unique_string: content
    }).then(function (data) {
        if (data.status) {
            var user = data.data[0].user;
            if (user.id) {
                success(user);
            } else {
                error();
            }
        } else {
            error();
        }
    });
});

Instascan.Camera.getCameras().then(function (cameras) {
    if (cameras.length > 1) {
        scanner.start(cameras[1]);
    }
    else if (cameras.length > 0) {
        scanner.start(cameras[0]);
    } else {
        console.error('No cameras found.');
    }
}).catch(function (e) {
    console.error(e);
    var cameraStatus            = document.getElementById('camera-status');
    cameraStatus.style.display  = 'block';
    cameraStatus.innerHTML      = 'Камера не найдена. Подключите камеру, и обновите страницу.';
});
