$(function(){
    // $('input').focusout(function(e) {
    //     $(this).parents('form').find('.error__custom').empty();
    //     $(this).parents('form').find('input').removeClass('error');
    //     $(this).parents('form').removeClass('show_error__ajax').removeClass('show_error__custom');
    // });
    $(window).on('ajaxErrorMessage', function(e, message){
        e.preventDefault();
        var alert = ''
        +'<div class="alert alert-danger alert-dismissible" role="alert">'
            +'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
            + message
        +'</div>';
        $('#errors').html(alert);
    })
    // $(window).on('ajaxError', function (e, data) {
    //     e.preventDefault();
    //     // if (typeof grecaptcha != 'undefined') {grecaptcha.reset()}
    //     $('form').find('.error__custom').empty();
    //     $('form').removeClass('show_error__ajax').removeClass('show_error__custom');
    //     if (data.status == 500) {
    //         if (data.responseText == 'signin') {
    //             $('#signin-form').addClass('show_error__ajax');
    //             $('#signin-form').find('input').addClass('error');
    //         }
    //     }
    //     if (data.status == 406) {
    //         try {
    //             var response = JSON.parse(data.responseText);
    //             if (response.X_OCTOBER_ERROR_FIELDS) {
    //                 var fields = response.X_OCTOBER_ERROR_FIELDS;
    //                 for (var field in fields) {
    //                     if (fields.hasOwnProperty(field)) {
    //                         $('[name=' + field + ']').addClass('error');
    //                         // $('[name=' + field + ']').parents('.form-group').removeClass('has-success').addClass('ajax-has-error');
    //                     }
    //                 }
    //             }
    //         } catch(e) {
    //             console.log('JSON parse error with status 406');
    //         }
    //     }
    // });

    $("[data-toggle=phone-mask]").mask("+7 (999) 999-99-99");
    $("[data-toggle=select-product]").change(function() {
        console.log('select', $(this).val(), $(this).find('option:selected').data('type'));
        var target = $(this).data('target'),
            type = $(this).find('option:selected').data('type');
        $(target).val(type);
    })
});
